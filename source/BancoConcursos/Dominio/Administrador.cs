﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Administrador
    {
        #region Propriedades

        public int AdministradorId { get; protected set; }

        public string Nome { get; protected set; }

        public string Email { get; protected set; }

        public string Senha { get; protected set; }

        public DateTime DataCadastro { get; protected set; }

        public enmStatusAdministrador Status { get; protected set; }

        #endregion

        #region Construtores

        public Administrador()
        {
            this.Status = enmStatusAdministrador.Ativo;
            this.DataCadastro = DateTime.Now;
        }

        public Administrador(string Nome, string Email, string Senha)
            : this()
        {
            this.Nome = Nome;
            this.Email = Email;
            this.Senha = Senha;
        }

        #endregion
    }

    #region Enum

    public enum enmStatusAdministrador
    {
        Inativo = 0,
        Ativo = 1
    }

    #endregion
}
