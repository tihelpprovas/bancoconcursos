﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Area
    {
        #region Propriedades

        public int AreaId { get; protected set; }
        public string Nome { get; protected set; }
        public DateTime DataCadastro { get; protected set; }
        public DateTime? DataAlteracao { get; protected set; }
        public virtual ICollection<Instituicao> Instituicoes { get; protected set; }

        #endregion

        #region Construtores
    
        public Area()
        {
            this.DataCadastro = DateTime.Now;
            this.Instituicoes = new List<Instituicao>();
        }

        public Area(string nome)
            : this()
        {
            this.Nome = nome;
        }

        #endregion

        #region Métodos
        public void Alterar(string nome)
        {
            this.Nome = nome;
            this.DataAlteracao = DateTime.Now;
        }
        #endregion
    }
}
