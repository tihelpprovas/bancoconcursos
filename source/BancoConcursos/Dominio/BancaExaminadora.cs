﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class BancaExaminadora
    {
        #region Propriedades

        public int BancaExaminadoraId { get; protected set; }
        public string Nome { get; protected set; }
        public string Sigla { get; protected set; }
        public string Tags { get; protected set; }

        #endregion

        #region Construtores

        public BancaExaminadora()
        {

        }

        public BancaExaminadora(string nome, string sigla, string tags)
        {
            Alterar(nome, sigla, tags);
        }

        #endregion

        #region Métodos
        public void Alterar(string nome, string sigla, string tags)
        {
            this.Nome = nome;
            this.Sigla = sigla;
            this.Tags = tags;
        }
        #endregion
    }
}
