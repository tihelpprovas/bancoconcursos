﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Cargo
    {
        #region Propriedades

        public int CargoId { get; protected set; }

        public string Nome { get; protected set; }

        public string NomeTotal
        {
            get
            {
                if(this.UsadoEmConcursos.HasValue && this.UsadoEmConcursos.Value > 0)
                    return this.Nome + " (" + this.UsadoEmConcursos.Value.ToString()  + ")";
                return this.Nome;
            }
        }

        public Decimal? Salario { get; protected set; }
        public int? CargaHoraria { get; protected set; }

        public int? UsadoEmConcursos { get; protected set; }

        public int? EscolaridadeId { get; protected set; }
        public virtual Escolaridade Escolaridade { get; protected set; }

        public virtual ICollection<ConcursoCargo> Concursos { get; protected set; }

        #endregion

        #region Construtores
        public Cargo()
        {
            this.Concursos = new List<ConcursoCargo>();
        }

        public Cargo(string nome, Decimal? salario, int? cargaHoraria, int? escolaridadeId)
            : this()
        {
            this.Nome = nome;
            this.Salario = salario;
            this.CargaHoraria = cargaHoraria;
            this.EscolaridadeId = escolaridadeId;
        }

        #endregion

        #region Métodos
        public void Alterar(string nome, Decimal? salario, int? cargaHoraria, int? escolaridadeId)
        {
            this.Nome = nome;
            this.Salario = salario;
            this.CargaHoraria = cargaHoraria;
            this.EscolaridadeId = escolaridadeId;
        }

        public void IncrementaUso()
        {
            if (this.UsadoEmConcursos.HasValue)
                this.UsadoEmConcursos = this.UsadoEmConcursos + 1;
            else
                this.UsadoEmConcursos = 1;
        }
        #endregion
    }
}
