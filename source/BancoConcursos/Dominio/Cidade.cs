﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Cidade
    {
        #region Propriedades

        public int CidadeId { get; protected set; }
        public string Nome { get; protected set; }
        public int EstadoId { get; protected set; }
        public virtual Estado Estado { get; protected set; }

        public virtual ICollection<Instituicao> Instituicoes { get; protected set; }
        public virtual ICollection<Concurso> Concursos { get; protected set; }

        #endregion

        #region Construtores
        public Cidade()
        {
            this.Instituicoes = new List<Instituicao>();
            this.Concursos = new List<Concurso>();
        }
        #endregion
    }
}
