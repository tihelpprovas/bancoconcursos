﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Concurso
    {
        #region Propriedades
        public int ConcursoId { get; protected set; }
        public string Descricao { get; protected set; }
        public int? Ano { get; protected set; }
        public int? QtdInscritos { get; protected set; }
        public string Tags { get; protected set; }
        public enmStatusConcurso Status { get; protected set; }
        public DateTime? DataValidadeConcurso { get; protected set; }
        public DateTime DataCadastro { get; protected set; }
        public DateTime? DataAlteracao { get; protected set; }
        public DateTime? DataCancelamento { get; protected set; }

        public int InstituicaoId { get; protected set; }
        public virtual Instituicao Instituicao { get; protected set; }
        public int BancaExaminadoraId { get; protected set; }
        public virtual BancaExaminadora BancaExaminadora { get; protected set; }

        public virtual ICollection<ConcursoEditalPublicado> ConcursosEditaisPublicados { get; protected set; }
        public virtual ICollection<Estado> Estados { get; protected set; }
        public virtual ICollection<Cidade> Cidades { get; protected set; }
        public virtual ICollection<ConcursoCargo> Cargos { get; protected set; }
        public virtual ICollection<ConcursoArquivo> Arquivos { get; protected set; }
        public virtual ICollection<ConcursoPrevisto> ConcursosPrevistos { get; protected set; }

        public ConcursoPrevisto ConcursoPrevistoAtual
        {
            get 
            {
                if(this.ConcursosPrevistos.Count > 0)
                    return this.ConcursosPrevistos.First();
                return null;
            }
        }

        public ConcursoEditalPublicado ConcursoEditalPublicado
        {
            get
            {
                if (this.ConcursosEditaisPublicados.Count > 0)
                    return this.ConcursosEditaisPublicados.First();
                return null;
            }
        }
        #endregion

        #region Construtores
        public Concurso()
        {
            this.DataCadastro = DateTime.Now;
            this.Status = enmStatusConcurso.Previsto;
            this.ConcursosEditaisPublicados = new List<ConcursoEditalPublicado>();
            this.Estados = new List<Estado>();
            this.Cidades = new List<Cidade>();
            this.Cargos = new List<ConcursoCargo>();
            this.Arquivos = new List<ConcursoArquivo>();
            this.ConcursosPrevistos = new List<ConcursoPrevisto>();
        }

        public Concurso(string descricao, int? ano, int? qtdInscritos, string tags, enmStatusConcurso status, DateTime? dataValidadeConcurso, DateTime? dataCancelamento, int instituicaoId, int bancaExaminadoraId)
            : this()
        {
            this.Descricao = descricao;
            this.Ano = ano;
            this.QtdInscritos = qtdInscritos;
            this.Tags = tags;
            this.Status = status;
            this.DataValidadeConcurso = DataValidadeConcurso;
            this.DataCancelamento = dataCancelamento;
            this.InstituicaoId = instituicaoId;
            this.BancaExaminadoraId = bancaExaminadoraId;
        }

        #endregion

        #region Metodos

        public void Alterar(string descricao, int? ano, int? qtdInscritos, string tags, enmStatusConcurso status, DateTime? dataValidadeConcurso, DateTime? dataCancelamento, int instituicaoId, int bancaExaminadoraId)
        {
            this.Descricao = descricao;
            this.Ano = ano;
            this.QtdInscritos = qtdInscritos;
            this.Tags = tags;
            this.Status = status;
            this.DataValidadeConcurso = DataValidadeConcurso;
            this.DataAlteracao = DateTime.Now;
            this.DataCancelamento = dataCancelamento;
            this.InstituicaoId = instituicaoId;
            this.BancaExaminadoraId = bancaExaminadoraId;
        }

        public string getStatus()
        {
            switch (this.Status)
            {
                case enmStatusConcurso.Previsto:
                    return "Previsto";
                case enmStatusConcurso.Autorizado:
                    return "Autorizado";
                case enmStatusConcurso.EditalPublicado:
                    return "Edital publicado";
                case enmStatusConcurso.InscricaoAberta:
                    return "Inscrições abertas";
                case enmStatusConcurso.InscricaoEncerrada:
                    return "Incrições encerradas";
                case enmStatusConcurso.Encerrado:
                    return "Encerrado";
                default:
                    return string.Empty;
            }
        }

        public List<DropDownListEnum> Situacoes()
        {
            var situations = new List<DropDownListEnum>();
            foreach (enmStatusConcurso item in Enum.GetValues(typeof(enmStatusConcurso)))
                situations.Add(new DropDownListEnum { Value = (int)item, Text = item.ToString() });
            return situations;
        }

        public void AdicionaEstado(Estado estado)
        {
            this.Estados.Add(estado);
        }

        public void RemoveEstados()
        {
            this.Estados.Clear();
        }

        public void AdicionaCidade(Cidade cidade)
        {
            this.Cidades.Add(cidade);
        }

        public void RemoveCidades()
        {
            this.Cidades.Clear();
        }

        #endregion
    }

    #region Enums

    public enum enmStatusConcurso
    {
        Previsto = 10,
        Autorizado = 20,
        EditalPublicado = 30,
        InscricaoAberta = 40,
        InscricaoEncerrada = 50,
        Encerrado = 60
    }

    #endregion
}

public struct DropDownListEnum
{
    public int Value { get; set; }

    public String Text { get; set; }

    public bool Selected { get; set; }
}