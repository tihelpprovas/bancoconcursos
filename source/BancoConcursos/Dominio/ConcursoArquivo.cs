﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class ConcursoArquivo
    {
        #region Propriedades

        public int ConcursoArquivoId { get; protected set; }

        public string NomeLegivel { get; protected set; }

        public string NomeArquivo { get; protected set; }

        public string Url { get; protected set; }

        public DateTime DataCadastro { get; protected set; }

        public enmTipoArquivoConcurso Tipo { get; protected set; }

        public int ConcursoId { get; protected set; }
        public virtual Concurso Concurso { get; protected set; }

        #endregion

        #region Construtores

        public ConcursoArquivo()
        {
            this.DataCadastro = DateTime.Now;
            this.Tipo = enmTipoArquivoConcurso.Edital;
        }

        #endregion
    }

    #region Enums

    public enum enmTipoArquivoConcurso
    {
        Edital = 1,
        Retificacao = 2,
        Gabarito = 3,
        Prova = 4,
        Autorizacao = 5
    }

    #endregion
}
