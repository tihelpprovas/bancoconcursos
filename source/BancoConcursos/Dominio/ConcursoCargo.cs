﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class ConcursoCargo
    {
        #region Propriedades
        public int ConcursoCargoId { get; protected set; }
        public int? Vagas { get; protected set; }
        public int? CadastroReserva { get; protected set; }
        public int? VagasDeficientes { get; protected set; }
        public int? VagasNegros { get; protected set; }
        public Decimal? TxInscricao { get; protected set; }
        public Decimal? Salario { get; protected set; }
        public int? CargaHoraria { get; protected set; }
        public DateTime? DataProvaObjetiva1 { get; protected set; }
        public DateTime? DataProvaObjetiva2 { get; protected set; }
        public DateTime? DataProvaPratica { get; protected set; }
        public DateTime? DataProvaTitulos { get; protected set; }
        public DateTime? DataProvaTAF { get; protected set; }
        public bool Horista { get; protected set; }

        public int ConcursoId { get; protected set; }
        public virtual Concurso Concurso { get; protected set; }
        public int CargoId { get; protected set; }
        public virtual Cargo Cargo { get; protected set; }
        public int EscolaridadeId { get; protected set; }
        public virtual Escolaridade Escolaridade { get; protected set; }

        public virtual ICollection<ConcursoCargoBeneficio> Beneficios { get; protected set; }

        #endregion

        #region Construtores

        public ConcursoCargo()
        {
            this.Beneficios = new List<ConcursoCargoBeneficio>();
        }

        public ConcursoCargo(Concurso concurso, 
            int cargoId,
            int escolaridadeId,
            int? vagas, 
            int? cadastroReserva, 
            int? vagasDeficientes, 
            int? vagasNegros, 
            Decimal? txInscricao, 
            Decimal? salario,
            int? cargaHoraria,
            DateTime? dataProvaObjetiva1,
            DateTime? dataProvaObjetiva2,
            DateTime? dataProvaPratica,
            DateTime? dataProvaTitulos,
            DateTime? dataProvaTAF,
            bool horista)
            : this()
        {
            this.Concurso = concurso;
            this.CargoId = cargoId;
            this.EscolaridadeId = escolaridadeId;
            this.Vagas = vagas;
            this.CadastroReserva = cadastroReserva;
            this.VagasDeficientes = vagasDeficientes;
            this.VagasNegros = vagasNegros;
            this.TxInscricao = txInscricao;
            this.Salario = salario;
            this.CargaHoraria = cargaHoraria;
            this.DataProvaObjetiva1 = dataProvaObjetiva1;
            this.DataProvaObjetiva2 = dataProvaObjetiva2;
            this.DataProvaPratica = dataProvaPratica;
            this.DataProvaTitulos = dataProvaTitulos;
            this.DataProvaTAF = dataProvaTAF;
            this.Horista = horista;
        }

        #endregion

        #region Metodos

        public void Alterar(int cargoId,
            int escolaridadeId,
            int? vagas, 
            int? cadastroReserva, 
            int? vagasDeficientes, 
            int? vagasNegros, 
            Decimal? txInscricao, 
            Decimal? salario,
            int? cargaHoraria,
            DateTime? dataProvaObjetiva1,
            DateTime? dataProvaObjetiva2,
            DateTime? dataProvaPratica,
            DateTime? dataProvaTitulos,
            DateTime? dataProvaTAF,
            bool horista)
        {
            this.CargoId = cargoId;
            this.EscolaridadeId = escolaridadeId;
            this.Vagas = vagas;
            this.CadastroReserva = cadastroReserva;
            this.VagasDeficientes = vagasDeficientes;
            this.VagasNegros = vagasNegros;
            this.TxInscricao = txInscricao;
            this.Salario = salario;
            this.CargaHoraria = cargaHoraria;
            this.DataProvaObjetiva1 = dataProvaObjetiva1;
            this.DataProvaObjetiva2 = dataProvaObjetiva2;
            this.DataProvaPratica = dataProvaPratica;
            this.DataProvaTitulos = dataProvaTitulos;
            this.DataProvaTAF = dataProvaTAF;
            this.Horista = horista;
        }

        #endregion
    }
}
