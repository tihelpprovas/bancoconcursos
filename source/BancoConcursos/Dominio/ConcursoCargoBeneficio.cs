﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class ConcursoCargoBeneficio
    {
        #region Propriedades
        public int ConcursoCargoBeneficioId { get; protected set; }
        public string Nome { get; protected set; }
        public Decimal Valor { get; protected set; }

        public int ConcursoCargoId { get; protected set; }
        public virtual ConcursoCargo ConcursoCargo { get; protected set; }

        #endregion

        #region Construtores
        #endregion
    }
}
