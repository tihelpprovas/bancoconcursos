﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class ConcursoEditalPublicado
    {
        #region Propriedades
        public int ConcursoEditalPublicadoId { get; protected set; }
        public DateTime DataInicioInscricao { get; protected set; }
        public DateTime DataFimInscricao { get; protected set; }

        public int ConcursoId { get; protected set; }
        public virtual Concurso Concurso { get; protected set; }

        #endregion

        #region Construtores

        public ConcursoEditalPublicado()
        {

        }

        public ConcursoEditalPublicado(Concurso concurso, DateTime dataInicioInscricao, DateTime dataFimInscricao)
        {
            this.Concurso = concurso;
            this.DataInicioInscricao = dataInicioInscricao;
            this.DataFimInscricao = dataFimInscricao;
        }

        public void Alterar(DateTime dataInicioInscricao, DateTime dataFimInscricao)
        {
            this.DataInicioInscricao = dataInicioInscricao;
            this.DataFimInscricao = dataFimInscricao;
        }

        #endregion
    }
}
