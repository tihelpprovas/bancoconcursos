﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class ConcursoPrevisto
    {
        #region Propriedades
        public int ConcursoPrevistoId { get; protected set; }
        public bool Especulacao { get; protected set; }
        public bool Solicitado { get; protected set; }
        public bool Autorizado { get; protected set; }
        public DateTime? DataEspeculacao { get; protected set; }
        public DateTime? DataSolicitacao { get; protected set; }
        public DateTime? DataAutorizacao { get; protected set; }

        public int ConcursoId { get; protected set; }
        public virtual Concurso Concurso { get; protected set; }

        #endregion

        #region Construtores

        public ConcursoPrevisto()
        {

        }

        public ConcursoPrevisto(Concurso concurso, bool especulacao, bool solicitado, bool autorizado, DateTime? dataEspeculacao, DateTime? dataSolicitacao, DateTime? dataAutorizacao)
        {
            this.Concurso = concurso;
            this.Especulacao = especulacao;
            this.Solicitado = solicitado;
            this.Autorizado = autorizado;
            this.DataEspeculacao = dataEspeculacao;
            this.DataSolicitacao = dataSolicitacao;
            this.DataAutorizacao = dataAutorizacao;
        }

        public void Alterar(bool especulacao, bool solicitado, bool autorizado, DateTime? dataEspeculacao, DateTime? dataSolicitacao, DateTime? dataAutorizacao)
        {
            this.Especulacao = especulacao;
            this.Solicitado = solicitado;
            this.Autorizado = autorizado;
            this.DataEspeculacao = dataEspeculacao;
            this.DataSolicitacao = dataSolicitacao;
            this.DataAutorizacao = dataAutorizacao;
        }

        #endregion
    }
}
