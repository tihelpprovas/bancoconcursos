﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Escolaridade
    {
        #region Propriedades
        public int EscolaridadeId { get; protected set; }
        public string Nome { get; protected set; }

        public virtual ICollection<ConcursoCargo> ConcursoCargos { get; protected set; }

        #endregion

        #region Construtores
        #endregion
    }
}
