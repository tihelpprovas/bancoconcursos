﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Estado
    {
        #region Propriedades

        public int EstadoId { get; protected set; }
        public string Nome { get; protected set; }
        public string Sigla { get; protected set; }
        public int RegiaoId { get; protected set; }
        public virtual Regiao Regiao { get; protected set; }

        public virtual ICollection<Concurso> Concursos { get; protected set; }
        public virtual ICollection<Cidade> Cidades { get; protected set; }
        public virtual ICollection<Instituicao> Instituicoes { get; protected set; }

        #endregion

        #region Construtores

        public Estado()
        {
            this.Concursos = new List<Concurso>();
            this.Cidades = new List<Cidade>();
            this.Instituicoes = new List<Instituicao>();
        }

        public Estado(int estadoId)
            : this()
        {
            this.EstadoId = estadoId;
        }

        #endregion

    }
}
