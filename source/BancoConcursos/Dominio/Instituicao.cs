﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    /// <summary>
    /// Instituicao pode ser de uma cidade ou um estado ou nacional (cidade e estado = null)
    /// </summary>
    public class Instituicao
    {
        #region Propriedades
        public int InstituicaoId { get; protected set; }
        public string Nome { get; protected set; }
        public string Sigla { get; protected set; }
        public string TextoDescritivo { get; protected set; }
        public string Tags { get; protected set; }
        public DateTime DataCadastro { get; protected set; }
        public DateTime? DataAlteracao { get; protected set; }
        public string Url { get; protected set; }
        public string Logradouro { get; protected set; }
        public string Numero { get; protected set; }
        public string Bairro { get; protected set; }

        public int AreaId { get; protected set; }
        public virtual Area Area { get; protected set; }
        public int? CidadeId { get; protected set; }
        public virtual Cidade Cidade { get; protected set; }
        public int? EstadoId { get; protected set; }
        public virtual Estado Estado { get; protected set; }

        public virtual ICollection<Concurso> Concursos { get; protected set; }

        public bool Desativado { get; protected set; }

        #endregion

        #region Construtores

        public Instituicao()
        {
            this.DataCadastro = DateTime.Now;
        }

        public Instituicao(string nome, string sigla, string textoDescritivo, string tags, int idArea, int? idCidade, int? idEstado, string url, string logradouro, string numero, string bairro)
            : this()
        {
            this.Nome = nome;
            this.Sigla = sigla;
            this.TextoDescritivo = textoDescritivo;
            this.Tags = tags;
            this.AreaId = idArea;
            this.CidadeId = idCidade;
            this.EstadoId = idEstado;
            this.Url = url;
            this.Logradouro = logradouro;
            this.Numero = numero;
            this.Bairro = bairro;

        }

        #endregion

        #region Métodos

        public void Alterar(string nome, string sigla, string textoDescritivo, string tags, int idArea, int? idCidade, int? idEstado, string url, string logradouro, string numero, string bairro)
        {
            this.Nome = nome;
            this.Sigla = sigla;
            this.TextoDescritivo = textoDescritivo;
            this.Tags = tags;
            this.DataAlteracao = DateTime.Now;
            this.AreaId = idArea;
            this.CidadeId = idCidade;
            this.EstadoId = idEstado;
            this.Url = url;
            this.Logradouro = logradouro;
            this.Numero = numero;
            this.Bairro = bairro;
        }

        public void Desativar()
        {
            this.Desativado = true;
        }

        #endregion
    }
}
