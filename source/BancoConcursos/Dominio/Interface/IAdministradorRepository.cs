﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio.Interface
{
    public interface IAdministradorRepository : IGenericRepository<Administrador>
    {
        Administrador Validar(string usuario, string senha);
    }
}
