﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominio
{
    public class Regiao
    {
        #region Propriedades
        public int RegiaoId { get; protected set; }
        public string Nome { get; protected set; }

        public virtual ICollection<Estado> Estado { get; protected set; }

        #endregion

        #region Construtores
        #endregion
    }
}
