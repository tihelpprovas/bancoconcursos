﻿using Dominio;
using Infra.EntityConfig;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Context
{
    public class Contexto : DbContext, IDisposable
    {
        public Contexto()
            : base("Contexto")
        {

        }

        public DbSet<Area> Area { get; set; }
        public DbSet<BancaExaminadora> BancaExaminadora { get; set; }
        public DbSet<Cargo> Cargo { get; set; }
        public DbSet<Cidade> Cidade { get; set; }
        public DbSet<Concurso> Concurso { get; set; }
        public DbSet<ConcursoArquivo> ConcursoArquivo { get; set; }
        public DbSet<ConcursoCargo> ConcursoCargo { get; set; }
        public DbSet<ConcursoCargoBeneficio> ConcursoCargoBeneficio { get; set; }
        public DbSet<ConcursoEditalPublicado> ConcursoEditalPublicado { get; set; }
        public DbSet<ConcursoPrevisto> ConcursoPrevisto { get; set; }
        public DbSet<Escolaridade> Escolaridade { get; set; }
        public DbSet<Estado> Estado { get; set; }
        public DbSet<Instituicao> Instituicao { get; set; }
        public DbSet<Regiao> Regiao { get; set; }
        public DbSet<Administrador> Administrador { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            // Ativando configuração das entidades
            modelBuilder.Configurations.Add(new AreaConfiguration());
            modelBuilder.Configurations.Add(new BancaExaminadoraConfiguration());
            modelBuilder.Configurations.Add(new CargoConfiguration());
            modelBuilder.Configurations.Add(new CidadeConfiguration());
            modelBuilder.Configurations.Add(new ConcursoArquivoConfiguration());
            modelBuilder.Configurations.Add(new ConcursoCargoBeneficioConfiguration());
            modelBuilder.Configurations.Add(new ConcursoConfiguration());
            modelBuilder.Configurations.Add(new ConcursoEditalPublicadoConfiguration());
            modelBuilder.Configurations.Add(new ConcursoPrevistoConfiguration());
            modelBuilder.Configurations.Add(new EscolaridadeConfiguration());
            modelBuilder.Configurations.Add(new EstadoConfiguration());
            modelBuilder.Configurations.Add(new InstituicaoConfiguration());
            modelBuilder.Configurations.Add(new RegiaoConfiguration());
            modelBuilder.Configurations.Add(new AdministradorConfiguration());

            // many-to-many
            modelBuilder.Entity<Concurso>()
                .HasMany<Estado>(s => s.Estados)
                .WithMany(c => c.Concursos)
                .Map(cs =>
                {
                    cs.MapLeftKey("ConcursoId");
                    cs.MapRightKey("EstadoId");
                    cs.ToTable("ConcursoEstado");
                });

            // many-to-many
            modelBuilder.Entity<Concurso>()
                .HasMany<Cidade>(s => s.Cidades)
                .WithMany(c => c.Concursos)
                .Map(cs =>
                {
                    cs.MapLeftKey("ConcursoId");
                    cs.MapRightKey("CidadeId");
                    cs.ToTable("ConcursoCidade");
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}
