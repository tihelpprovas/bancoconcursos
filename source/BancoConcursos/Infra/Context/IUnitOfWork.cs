﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Context
{
    public interface IUnitOfWork
    {
        Contexto Get();
    }
}
