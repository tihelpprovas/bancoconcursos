﻿using Dominio.Interface;
using Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Context
{
    public class Repository : IDisposable
    {
        private readonly IUnitOfWork unitOfWork;
        public Repository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        private IAreaRepository areaRepository;
        private IBancaExaminadoraRepository bancaExaminadoraRepository;
        private ICargoRepository cargoRepository;
        private ICidadeRepository cidadeRepository;
        private IConcursoRepository concursoRepository;
        private IConcursoArquivoRepository concursoArquivoRepository;
        private IConcursoCargoRepository concursoCargoRepository;
        private IConcursoCargoBeneficioRepository concursoCargoBeneficioRepository;
        private IConcursoEditalPublicadoRepository concursoEditalPublicadoRepository;
        private IConcursoPrevistoRepository concursoPrevistoRepository;
        private IEscolaridadeRepository escolaridadeRepository;
        private IEstadoRepository estadoRepository;
        private IInstituicaoRepository instituicaoRepository;
        private IRegiaoRepository regiaoRepository;
        private IAdministradorRepository administradorRepository;

        public IAreaRepository AreaRepository
        {
            get
            {
                if (this.areaRepository == null)
                    this.areaRepository = new AreaRepository(this.unitOfWork);
                return areaRepository;
            }
        }

        public IBancaExaminadoraRepository BancaExaminadoraRepository
        {
            get
            {
                if (this.bancaExaminadoraRepository == null)
                    this.bancaExaminadoraRepository = new BancaExaminadoraRepository(this.unitOfWork);
                return bancaExaminadoraRepository;
            }
        }

        public ICargoRepository CargoRepository
        {
            get
            {
                if (this.cargoRepository == null)
                    this.cargoRepository = new CargoRepository(this.unitOfWork);
                return cargoRepository;
            }
        }

        public ICidadeRepository CidadeRepository
        {
            get
            {
                if (this.cidadeRepository == null)
                    this.cidadeRepository = new CidadeRepository(this.unitOfWork);
                return cidadeRepository;
            }
        }

        public IConcursoRepository ConcursoRepository
        {
            get
            {
                if (this.concursoRepository == null)
                    this.concursoRepository = new ConcursoRepository(this.unitOfWork);
                return concursoRepository;
            }
        }

        public IConcursoArquivoRepository ConcursoArquivoRepository
        {
            get
            {
                if (this.concursoArquivoRepository == null)
                    this.concursoArquivoRepository = new ConcursoArquivoRepository(this.unitOfWork);
                return concursoArquivoRepository;
            }
        }

        public IConcursoCargoRepository ConcursoCargoRepository
        {
            get
            {
                if (this.concursoCargoRepository == null)
                    this.concursoCargoRepository = new ConcursoCargoRepository(this.unitOfWork);
                return concursoCargoRepository;
            }
        }

        public IConcursoCargoBeneficioRepository ConcursoCargoBeneficioRepository
        {
            get
            {
                if (this.concursoCargoBeneficioRepository == null)
                    this.concursoCargoBeneficioRepository = new ConcursoCargoBeneficioRepository(this.unitOfWork);
                return concursoCargoBeneficioRepository;
            }
        }

        public IConcursoEditalPublicadoRepository ConcursoEditalPublicadoRepository
        {
            get
            {
                if (this.concursoEditalPublicadoRepository == null)
                    this.concursoEditalPublicadoRepository = new ConcursoEditalPublicadoRepository(this.unitOfWork);
                return concursoEditalPublicadoRepository;
            }
        }

        public IConcursoPrevistoRepository ConcursoPrevistoRepository
        {
            get
            {
                if (this.concursoPrevistoRepository == null)
                    this.concursoPrevistoRepository = new ConcursoPrevistoRepository(this.unitOfWork);
                return concursoPrevistoRepository;
            }
        }

        public IEscolaridadeRepository EscolaridadeRepository
        {
            get
            {
                if (this.escolaridadeRepository == null)
                    this.escolaridadeRepository = new EscolaridadeRepository(this.unitOfWork);
                return escolaridadeRepository;
            }
        }

        public IEstadoRepository EstadoRepository
        {
            get
            {
                if (this.estadoRepository == null)
                    this.estadoRepository = new EstadoRepository(this.unitOfWork);
                return estadoRepository;
            }
        }

        public IInstituicaoRepository InstituicaoRepository
        {
            get
            {
                if (this.instituicaoRepository == null)
                    this.instituicaoRepository = new InstituicaoRepository(this.unitOfWork);
                return instituicaoRepository;
            }
        }

        public IRegiaoRepository RegiaoRepository
        {
            get
            {
                if (this.regiaoRepository == null)
                    this.regiaoRepository = new RegiaoRepository(this.unitOfWork);
                return regiaoRepository;
            }
        }

        public IAdministradorRepository AdministradorRepository
        {
            get
            {
                if (this.administradorRepository == null)
                    this.administradorRepository = new AdministradorRepository(this.unitOfWork);
                return administradorRepository;
            }
        }

        public bool Save()
        {
            return this.unitOfWork.Get().SaveChanges() == 1;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.unitOfWork.Get().Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
