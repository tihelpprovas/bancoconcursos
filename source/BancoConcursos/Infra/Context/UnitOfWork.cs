﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Context
{
    public class UnitOfWork : IUnitOfWork
    {
        private Contexto _contexto;
        public Contexto Get()
        {
            if (_contexto == null)
                _contexto = new Contexto();
            return _contexto;
        }
    }
}
