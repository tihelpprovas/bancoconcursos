﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Infra.CookieManager
{
    public static class Cookie
    {
        #region Métodos Private

        private static string setValor(string valor)
        {
            return valor.ToCryptQueryString();
        }

        private static string getValor(string valor)
        {
            return valor.ToDecryptQueryString();
        }

        #endregion

        public static void RemoverCookie(string nomeCookie)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        public static void SetCookie(string valor, string nomeCookie)
        {
            HttpCookie cookie = new HttpCookie(nomeCookie);
            cookie.Value = setValor(valor);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void SetCookie(string nomeCookie, string parametro, string valor)
        {
            parametro = parametro.ToCryptQueryString();
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
            {
                if (!string.IsNullOrEmpty(cookie.Values.Get(parametro)))
                    cookie.Values.Set(parametro, setValor(valor));
                else
                    cookie.Values.Add(parametro, setValor(valor));
            }
            else
            {
                cookie = new HttpCookie(nomeCookie);
                cookie.Values.Add(parametro, setValor(valor));
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static string GetCookie(string nomeCookie)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
                return getValor(cookie.Value);
            return string.Empty;
        }

        public static string GetCookie(string nomeCookie, string parametro)
        {
            parametro = parametro.ToCryptQueryString();
            HttpCookie cookie = HttpContext.Current.Request.Cookies[nomeCookie];
            if (cookie != null)
            {
                if (!string.IsNullOrEmpty(cookie.Values.Get(parametro)))
                    return getValor(cookie.Values.Get(parametro));
            }
            return string.Empty;
        }
    }

}
