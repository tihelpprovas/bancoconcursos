﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.EntityConfig
{
    public class AdministradorConfiguration : EntityTypeConfiguration<Administrador>
    {
        public AdministradorConfiguration()
        {
            Property(x => x.Nome).IsRequired().HasMaxLength(100);
            Property(x => x.Email).IsRequired().HasMaxLength(100);
            Property(x => x.Senha).IsRequired().HasMaxLength(200);
        }
    }
}
