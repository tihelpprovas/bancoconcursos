﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.EntityConfig
{
    public class ConcursoArquivoConfiguration : EntityTypeConfiguration<ConcursoArquivo>
    {
        public ConcursoArquivoConfiguration()
        {
            Property(x => x.NomeLegivel).IsRequired().HasMaxLength(50);
            Property(x => x.NomeArquivo).IsRequired().HasMaxLength(50);
            Property(x => x.Url).IsRequired().HasMaxLength(500);
        }
    }
}
