﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.EntityConfig
{
    public class ConcursoEditalPublicadoConfiguration : EntityTypeConfiguration<ConcursoEditalPublicado>
    {
        public ConcursoEditalPublicadoConfiguration()
        {
            Property(x => x.DataInicioInscricao).IsOptional();
            Property(x => x.DataFimInscricao).IsOptional();
        }
    }
}
