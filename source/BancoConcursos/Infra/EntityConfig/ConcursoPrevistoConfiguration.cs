﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.EntityConfig
{
    public class ConcursoPrevistoConfiguration : EntityTypeConfiguration<ConcursoPrevisto>
    {
        public ConcursoPrevistoConfiguration()
        {
            Property(x => x.DataAutorizacao).IsOptional();
            Property(x => x.DataEspeculacao).IsOptional();
            Property(x => x.DataSolicitacao).IsOptional();
        }
    }
}
