﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.EntityConfig
{
    public class InstituicaoConfiguration : EntityTypeConfiguration<Instituicao>
    {
        public InstituicaoConfiguration()
        {
            Property(x => x.Nome).IsRequired().HasMaxLength(500);
            Property(x => x.Sigla).HasMaxLength(50);
            Property(x => x.Url).IsOptional().HasMaxLength(300);
            Property(x => x.Logradouro).IsOptional().HasMaxLength(200);
            Property(x => x.Numero).IsOptional().HasMaxLength(10);
            Property(x => x.Bairro).IsOptional().HasMaxLength(200);
        }
    }
}
