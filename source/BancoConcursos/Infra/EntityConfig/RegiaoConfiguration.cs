﻿using Dominio;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.EntityConfig
{
    public class RegiaoConfiguration : EntityTypeConfiguration<Regiao>
    {
        public RegiaoConfiguration()
        {
            Property(x => x.Nome).IsRequired().HasMaxLength(500);
        }
    }
}
