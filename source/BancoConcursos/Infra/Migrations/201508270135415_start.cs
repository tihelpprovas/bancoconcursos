namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Administrador",
                c => new
                    {
                        AdministradorId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 100),
                        Senha = c.String(nullable: false, maxLength: 200),
                        DataCadastro = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdministradorId);
            
            CreateTable(
                "dbo.Area",
                c => new
                    {
                        AreaId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 500),
                        DataCadastro = c.DateTime(nullable: false),
                        DataAlteracao = c.DateTime(),
                    })
                .PrimaryKey(t => t.AreaId);
            
            CreateTable(
                "dbo.Instituicao",
                c => new
                    {
                        InstituicaoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 500),
                        Sigla = c.String(maxLength: 50),
                        TextoDescritivo = c.String(),
                        Tags = c.String(),
                        DataCadastro = c.DateTime(nullable: false),
                        DataAlteracao = c.DateTime(),
                        AreaId = c.Int(),
                        CidadeId = c.Int(),
                        EstadoId = c.Int(),
                    })
                .PrimaryKey(t => t.InstituicaoId)
                .ForeignKey("dbo.Area", t => t.AreaId)
                .ForeignKey("dbo.Estado", t => t.EstadoId)
                .ForeignKey("dbo.Cidade", t => t.CidadeId)
                .Index(t => t.AreaId)
                .Index(t => t.EstadoId)
                .Index(t => t.CidadeId);
            
            CreateTable(
                "dbo.Cidade",
                c => new
                    {
                        CidadeId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 500),
                        EstadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CidadeId)
                .ForeignKey("dbo.Estado", t => t.EstadoId)
                .Index(t => t.EstadoId);
            
            CreateTable(
                "dbo.Estado",
                c => new
                    {
                        EstadoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 500),
                        Sigla = c.String(maxLength: 2),
                        RegiaoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EstadoId)
                .ForeignKey("dbo.Regiao", t => t.RegiaoId)
                .Index(t => t.RegiaoId);
            
            CreateTable(
                "dbo.Concurso",
                c => new
                    {
                        ConcursoId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false),
                        Ano = c.Int(),
                        QtdInscritos = c.Int(),
                        Tags = c.String(),
                        Status = c.Int(nullable: false),
                        DataValidadeConcurso = c.DateTime(),
                        DataCadastro = c.DateTime(nullable: false),
                        DataAlteracao = c.DateTime(),
                        DataCancelamento = c.DateTime(),
                        InstituicaoId = c.Int(nullable: false),
                        BancaExaminadoraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ConcursoId)
                .ForeignKey("dbo.BancaExaminadora", t => t.BancaExaminadoraId)
                .ForeignKey("dbo.Instituicao", t => t.InstituicaoId)
                .Index(t => t.BancaExaminadoraId)
                .Index(t => t.InstituicaoId);
            
            CreateTable(
                "dbo.ConcursoArquivo",
                c => new
                    {
                        ConcursoArquivoId = c.Int(nullable: false, identity: true),
                        NomeLegivel = c.String(nullable: false, maxLength: 50),
                        NomeArquivo = c.String(nullable: false, maxLength: 50),
                        Url = c.String(nullable: false, maxLength: 500),
                        DataCadastro = c.DateTime(nullable: false),
                        Tipo = c.Int(nullable: false),
                        ConcursoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ConcursoArquivoId)
                .ForeignKey("dbo.Concurso", t => t.ConcursoId)
                .Index(t => t.ConcursoId);
            
            CreateTable(
                "dbo.BancaExaminadora",
                c => new
                    {
                        BancaExaminadoraId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 500),
                        Sigla = c.String(maxLength: 50),
                        Tags = c.String(),
                    })
                .PrimaryKey(t => t.BancaExaminadoraId);
            
            CreateTable(
                "dbo.ConcursoCargo",
                c => new
                    {
                        ConcursoCargoId = c.Int(nullable: false, identity: true),
                        Vagas = c.Int(),
                        CadastroReserva = c.Int(),
                        VagasDeficientes = c.Int(),
                        VagasNegros = c.Int(),
                        TxInscricao = c.Decimal(precision: 18, scale: 2),
                        Salario = c.Decimal(precision: 18, scale: 2),
                        CargaHoraria = c.Int(),
                        DataProvaObjetiva1 = c.DateTime(),
                        DataProvaObjetiva2 = c.DateTime(),
                        DataProvaPratica = c.DateTime(),
                        DataProvaTitulos = c.DateTime(),
                        DataProvaTAF = c.DateTime(),
                        ConcursoId = c.Int(nullable: false),
                        CargoId = c.Int(nullable: false),
                        EscolaridadeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ConcursoCargoId)
                .ForeignKey("dbo.Cargo", t => t.CargoId)
                .ForeignKey("dbo.Concurso", t => t.ConcursoId)
                .ForeignKey("dbo.Escolaridade", t => t.EscolaridadeId)
                .Index(t => t.CargoId)
                .Index(t => t.ConcursoId)
                .Index(t => t.EscolaridadeId);
            
            CreateTable(
                "dbo.ConcursoCargoBeneficio",
                c => new
                    {
                        ConcursoCargoBeneficioId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 500),
                        Valor = c.Decimal(precision: 18, scale: 2),
                        ConcursoCargoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ConcursoCargoBeneficioId)
                .ForeignKey("dbo.ConcursoCargo", t => t.ConcursoCargoId)
                .Index(t => t.ConcursoCargoId);
            
            CreateTable(
                "dbo.Cargo",
                c => new
                    {
                        CargoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.CargoId);
            
            CreateTable(
                "dbo.Escolaridade",
                c => new
                    {
                        EscolaridadeId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.EscolaridadeId);
            
            CreateTable(
                "dbo.ConcursoPrevisto",
                c => new
                    {
                        ConcursoPrevistoId = c.Int(nullable: false, identity: true),
                        Especulacao = c.Boolean(nullable: false),
                        Solicitado = c.Boolean(nullable: false),
                        Autorizado = c.Boolean(nullable: false),
                        DataEspeculacao = c.DateTime(),
                        DataSolicitacao = c.DateTime(),
                        DataAutorizacao = c.DateTime(),
                        ConcursoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ConcursoPrevistoId)
                .ForeignKey("dbo.Concurso", t => t.ConcursoId)
                .Index(t => t.ConcursoId);
            
            CreateTable(
                "dbo.ConcursoEditalPublicado",
                c => new
                    {
                        ConcursoEditalPublicadoId = c.Int(nullable: false, identity: true),
                        DataInicioInscricao = c.DateTime(),
                        DataFimInscricao = c.DateTime(),
                        ConcursoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ConcursoEditalPublicadoId)
                .ForeignKey("dbo.Concurso", t => t.ConcursoId)
                .Index(t => t.ConcursoId);
            
            CreateTable(
                "dbo.Regiao",
                c => new
                    {
                        RegiaoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 500),
                    })
                .PrimaryKey(t => t.RegiaoId);
            
            CreateTable(
                "dbo.ConcursoEstado",
                c => new
                    {
                        Concurso_ConcursoId = c.Int(nullable: false),
                        Estado_EstadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Concurso_ConcursoId, t.Estado_EstadoId })
                .ForeignKey("dbo.Concurso", t => t.Concurso_ConcursoId)
                .ForeignKey("dbo.Estado", t => t.Estado_EstadoId)
                .Index(t => t.Concurso_ConcursoId)
                .Index(t => t.Estado_EstadoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Instituicao", "CidadeId", "dbo.Cidade");
            DropForeignKey("dbo.Estado", "RegiaoId", "dbo.Regiao");
            DropForeignKey("dbo.Instituicao", "EstadoId", "dbo.Estado");
            DropForeignKey("dbo.Concurso", "InstituicaoId", "dbo.Instituicao");
            DropForeignKey("dbo.ConcursoEstado", "Estado_EstadoId", "dbo.Estado");
            DropForeignKey("dbo.ConcursoEstado", "Concurso_ConcursoId", "dbo.Concurso");
            DropForeignKey("dbo.ConcursoEditalPublicado", "ConcursoId", "dbo.Concurso");
            DropForeignKey("dbo.ConcursoPrevisto", "ConcursoId", "dbo.Concurso");
            DropForeignKey("dbo.ConcursoCargo", "EscolaridadeId", "dbo.Escolaridade");
            DropForeignKey("dbo.ConcursoCargo", "ConcursoId", "dbo.Concurso");
            DropForeignKey("dbo.ConcursoCargo", "CargoId", "dbo.Cargo");
            DropForeignKey("dbo.ConcursoCargoBeneficio", "ConcursoCargoId", "dbo.ConcursoCargo");
            DropForeignKey("dbo.Concurso", "BancaExaminadoraId", "dbo.BancaExaminadora");
            DropForeignKey("dbo.ConcursoArquivo", "ConcursoId", "dbo.Concurso");
            DropForeignKey("dbo.Cidade", "EstadoId", "dbo.Estado");
            DropForeignKey("dbo.Instituicao", "AreaId", "dbo.Area");
            DropIndex("dbo.Instituicao", new[] { "CidadeId" });
            DropIndex("dbo.Estado", new[] { "RegiaoId" });
            DropIndex("dbo.Instituicao", new[] { "EstadoId" });
            DropIndex("dbo.Concurso", new[] { "InstituicaoId" });
            DropIndex("dbo.ConcursoEstado", new[] { "Estado_EstadoId" });
            DropIndex("dbo.ConcursoEstado", new[] { "Concurso_ConcursoId" });
            DropIndex("dbo.ConcursoEditalPublicado", new[] { "ConcursoId" });
            DropIndex("dbo.ConcursoPrevisto", new[] { "ConcursoId" });
            DropIndex("dbo.ConcursoCargo", new[] { "EscolaridadeId" });
            DropIndex("dbo.ConcursoCargo", new[] { "ConcursoId" });
            DropIndex("dbo.ConcursoCargo", new[] { "CargoId" });
            DropIndex("dbo.ConcursoCargoBeneficio", new[] { "ConcursoCargoId" });
            DropIndex("dbo.Concurso", new[] { "BancaExaminadoraId" });
            DropIndex("dbo.ConcursoArquivo", new[] { "ConcursoId" });
            DropIndex("dbo.Cidade", new[] { "EstadoId" });
            DropIndex("dbo.Instituicao", new[] { "AreaId" });
            DropTable("dbo.ConcursoEstado");
            DropTable("dbo.Regiao");
            DropTable("dbo.ConcursoEditalPublicado");
            DropTable("dbo.ConcursoPrevisto");
            DropTable("dbo.Escolaridade");
            DropTable("dbo.Cargo");
            DropTable("dbo.ConcursoCargoBeneficio");
            DropTable("dbo.ConcursoCargo");
            DropTable("dbo.BancaExaminadora");
            DropTable("dbo.ConcursoArquivo");
            DropTable("dbo.Concurso");
            DropTable("dbo.Estado");
            DropTable("dbo.Cidade");
            DropTable("dbo.Instituicao");
            DropTable("dbo.Area");
            DropTable("dbo.Administrador");
        }
    }
}
