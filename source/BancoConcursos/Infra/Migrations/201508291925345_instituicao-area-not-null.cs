namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class instituicaoareanotnull : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Instituicao", "AreaId", "dbo.Area");
            DropIndex("dbo.Instituicao", new[] { "AreaId" });
            AlterColumn("dbo.Instituicao", "AreaId", c => c.Int(nullable: false));
            CreateIndex("dbo.Instituicao", "AreaId");
            AddForeignKey("dbo.Instituicao", "AreaId", "dbo.Area", "AreaId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Instituicao", "AreaId", "dbo.Area");
            DropIndex("dbo.Instituicao", new[] { "AreaId" });
            AlterColumn("dbo.Instituicao", "AreaId", c => c.Int());
            CreateIndex("dbo.Instituicao", "AreaId");
            AddForeignKey("dbo.Instituicao", "AreaId", "dbo.Area", "AreaId");
        }
    }
}
