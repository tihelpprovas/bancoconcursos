namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addconcursoestadocidade : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ConcursoEstado", name: "Concurso_ConcursoId", newName: "ConcursoId");
            RenameColumn(table: "dbo.ConcursoEstado", name: "Estado_EstadoId", newName: "EstadoId");
            CreateTable(
                "dbo.ConcursoCidade",
                c => new
                    {
                        ConcursoId = c.Int(nullable: false),
                        CidadeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ConcursoId, t.CidadeId })
                .ForeignKey("dbo.Concurso", t => t.ConcursoId)
                .ForeignKey("dbo.Cidade", t => t.CidadeId)
                .Index(t => t.ConcursoId)
                .Index(t => t.CidadeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ConcursoCidade", "CidadeId", "dbo.Cidade");
            DropForeignKey("dbo.ConcursoCidade", "ConcursoId", "dbo.Concurso");
            DropIndex("dbo.ConcursoCidade", new[] { "CidadeId" });
            DropIndex("dbo.ConcursoCidade", new[] { "ConcursoId" });
            DropTable("dbo.ConcursoCidade");
            RenameColumn(table: "dbo.ConcursoEstado", name: "EstadoId", newName: "Estado_EstadoId");
            RenameColumn(table: "dbo.ConcursoEstado", name: "ConcursoId", newName: "Concurso_ConcursoId");
        }
    }
}
