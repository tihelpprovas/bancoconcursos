namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cargonomesize : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cargo", "Nome", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cargo", "Nome", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
