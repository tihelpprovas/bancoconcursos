// <auto-generated />
namespace Infra.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class cargonomesize150 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(cargonomesize150));
        
        string IMigrationMetadata.Id
        {
            get { return "201509091657189_cargo-nome-size-150"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
