namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cargopresets : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cargo", "Salario", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Cargo", "CargaHoraria", c => c.Int());
            AddColumn("dbo.Cargo", "EscolaridadeId", c => c.Int());
            CreateIndex("dbo.Cargo", "EscolaridadeId");
            AddForeignKey("dbo.Cargo", "EscolaridadeId", "dbo.Escolaridade", "EscolaridadeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cargo", "EscolaridadeId", "dbo.Escolaridade");
            DropIndex("dbo.Cargo", new[] { "EscolaridadeId" });
            DropColumn("dbo.Cargo", "EscolaridadeId");
            DropColumn("dbo.Cargo", "CargaHoraria");
            DropColumn("dbo.Cargo", "Salario");
        }
    }
}
