namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class instituicaourlbairro : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Instituicao", "Url", c => c.String(maxLength: 300));
            AddColumn("dbo.Instituicao", "Logradouro", c => c.String(maxLength: 200));
            AddColumn("dbo.Instituicao", "Numero", c => c.String(maxLength: 10));
            AddColumn("dbo.Instituicao", "Bairro", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Instituicao", "Bairro");
            DropColumn("dbo.Instituicao", "Numero");
            DropColumn("dbo.Instituicao", "Logradouro");
            DropColumn("dbo.Instituicao", "Url");
        }
    }
}
