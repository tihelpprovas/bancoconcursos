namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class concursocargohorista : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ConcursoCargo", "Horista", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ConcursoCargo", "Horista");
        }
    }
}
