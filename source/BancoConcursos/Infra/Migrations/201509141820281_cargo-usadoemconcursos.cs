namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cargousadoemconcursos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cargo", "UsadoEmConcursos", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cargo", "UsadoEmConcursos");
        }
    }
}
