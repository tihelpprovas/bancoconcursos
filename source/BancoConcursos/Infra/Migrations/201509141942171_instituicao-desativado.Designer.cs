// <auto-generated />
namespace Infra.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class instituicaodesativado : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(instituicaodesativado));
        
        string IMigrationMetadata.Id
        {
            get { return "201509141942171_instituicao-desativado"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
