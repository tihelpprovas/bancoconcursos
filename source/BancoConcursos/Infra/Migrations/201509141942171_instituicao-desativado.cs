namespace Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class instituicaodesativado : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Instituicao", "Desativado", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Instituicao", "Desativado");
        }
    }
}
