﻿using Dominio;
using Dominio.Interface;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repository
{
    public class AdministradorRepository : GenericRepository<Administrador>, IAdministradorRepository
    {
        public AdministradorRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public Administrador Validar(string usuario, string senha)
        {
            senha = senha.ToCrypt();
            return this.Get(x => x.Email == usuario && x.Senha == senha);
        }
    }
}
