﻿using Dominio;
using Dominio.Interface;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repository
{
    public class ConcursoEditalPublicadoRepository : GenericRepository<ConcursoEditalPublicado>, IConcursoEditalPublicadoRepository
    {
        public ConcursoEditalPublicadoRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }
}
