﻿using Dominio;
using Dominio.Interface;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repository
{
    public class ConcursoPrevistoRepository : GenericRepository<ConcursoPrevisto>, IConcursoPrevistoRepository
    {
        public ConcursoPrevistoRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }
    }
}
