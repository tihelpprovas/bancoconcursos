﻿using Dominio.Interface;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Repository
{
    public class GenericRepository<T> : IDisposable, IGenericRepository<T> where T : class
    {
        private Contexto _contexto;
        protected readonly DbSet<T> dbset;

        protected GenericRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            dbset = getContexto.Set<T>();
        }

        protected IUnitOfWork _unitOfWork
        {
            get;
            private set;
        }

        protected Contexto getContexto
        {
            get { return _contexto ?? (_contexto = _unitOfWork.Get()); }
        }

        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            _contexto.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            dbset.Remove(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = dbset.Where<T>(where).AsEnumerable();
            foreach (T obj in objects)
                dbset.Remove(obj);
        }

        public virtual T GetById(int id)
        {
            return dbset.Find(id);
        }

        public virtual IQueryable<T> GetAll()
        {
            return dbset;
        }

        public virtual IQueryable<T> GetAll(Func<T, object> order, int pageSize, int pageIndex, out int totalPages)
        {
            totalPages = dbset.Count();
            return dbset.OrderBy(order).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsQueryable();
        }

        public virtual IQueryable<T> GetAllDescending(Func<T, object> order, int pageSize, int pageIndex, out int totalPages)
        {
            totalPages = dbset.Count();
            return dbset.OrderByDescending(order).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsQueryable();
        }

        public virtual IQueryable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where);
        }

        public virtual IQueryable<T> GetMany(Expression<Func<T, bool>> where, Func<T, object> order, int pageSize, int pageIndex, out int totalPages)
        {
            totalPages = dbset.Where(where).Count();
            return dbset.Where(where).OrderBy(order).Skip((pageIndex - 1) * pageSize).Take(pageSize).AsQueryable();
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).FirstOrDefault<T>();
        }

        public int Count(Expression<Func<T, bool>> where)
        {
            return dbset.Count(where);
        }

        public void Reload(T entity)
        {
            _contexto.Entry<T>(entity).Reload();
        }

        public void Reload(T entity, Expression<Func<T, object>> attribute)
        {
            _contexto.Entry<T>(entity).Reference(attribute).Load();
        }

        public void Dispose()
        {
            _contexto.Dispose();
        }
    }

}
