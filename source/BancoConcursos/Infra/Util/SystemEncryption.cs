﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    public static class SystemEncryption
    {
        #region Atributos

        private static string chave = "HP=Z8#P@0!15/8$%3K-9CCONC*FDH+0Z";
        private static SymmetricAlgorithm algoritmo = new RijndaelManaged();

        #endregion

        #region Métodos

        #region Métodos privados

        private static string base64Encode(string data)
        {
            byte[] encData_byte = new byte[data.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(data);
            string encodedData = Convert.ToBase64String(encData_byte);
            return encodedData;
        }

        private static string base64Decode(string data)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            Decoder utf8Decode = encoder.GetDecoder();

            byte[] todecode_byte = Convert.FromBase64String(data);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }

        private static string Criptografa(string valor, string chave)
        {
            byte[] ByteValor = Encoding.UTF8.GetBytes(valor);

            // Seta a chave privada
            algoritmo.Mode = CipherMode.CBC;
            algoritmo.Key = Encoding.Default.GetBytes(chave);
            algoritmo.IV = Encoding.Default.GetBytes("concurseiro-2015");

            // Interface de criptografia / Cria objeto de criptografia
            ICryptoTransform cryptoTransform = algoritmo.CreateEncryptor();

            MemoryStream _memoryStream = new MemoryStream();
            CryptoStream _cryptoStream = new CryptoStream(_memoryStream, cryptoTransform, CryptoStreamMode.Write);

            // Grava os dados criptografados no MemoryStream
            _cryptoStream.Write(ByteValor, 0, ByteValor.Length);
            _cryptoStream.FlushFinalBlock();

            // Busca o tamanho dos bytes encriptados
            byte[] cryptoByte = _memoryStream.ToArray();

            // Converte para a base 64 string para uso posterior em um xml
            return Convert.ToBase64String(cryptoByte, 0, cryptoByte.GetLength(0));
        }

        private static string Descriptografa(string valor, string chave)
        {
            // Converte a base 64 string em num array de bytes
            byte[] cryptoByte = Convert.FromBase64String(valor);

            // Seta a chave privada
            algoritmo.Mode = CipherMode.CBC;
            algoritmo.Key = Encoding.Default.GetBytes(chave);
            algoritmo.IV = Encoding.Default.GetBytes("concurseiro-2015");

            // Interface de criptografia / Cria objeto de descriptografia
            ICryptoTransform cryptoTransform = algoritmo.CreateDecryptor();

            MemoryStream _memoryStream = new MemoryStream(cryptoByte, 0, cryptoByte.Length);
            CryptoStream _cryptoStream = new CryptoStream(_memoryStream, cryptoTransform, CryptoStreamMode.Read);

            // Busca resultado do CryptoStream
            StreamReader _streamReader = new StreamReader(_cryptoStream);
            return _streamReader.ReadToEnd();
        }

        #endregion

        public static string ToCrypt(this string valor)
        {
            return Criptografa(valor, chave);
        }

        public static string ToDecrypt(this string valor)
        {
            return Descriptografa(valor, chave);
        }

        public static string ToCryptQueryString(this string valor)
        {
            return base64Encode(Criptografa(valor, chave));
        }

        public static string ToDecryptQueryString(this string valor)
        {
            return Descriptografa(base64Decode(valor), chave);
        }

        #endregion
    }

}
