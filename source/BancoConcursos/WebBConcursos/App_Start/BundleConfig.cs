﻿using System.Web;
using System.Web.Optimization;

namespace WebBConcursos
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*", 
            //            "~/Scripts/methods_pt.js",
            //            "~/Scripts/jquery.validate.custom.pt-br.js"));

            // CSS
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/alertify.core.css",
                "~/Content/alertify.default.css",
                "~/Content/style.css",
                "~/Content/style-responsive.css",
                "~/Content/fonts.css",
                "~/Content/multiple-select.css"));

            // data-tables
            bundles.Add(new StyleBundle("~/Content/data-tables").Include(
                "~/Content/plugins/data-tables/DT_bootstrap.css",
                "~/Content/plugins/advanced-datatable/css/demo_table.css",
                "~/Content/plugins/advanced-datatable/css/demo_page.css"));

            // SCRIPTS
            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/alertify.js",
                        "~/Scripts/JScript.js",
                        "~/Scripts/util.js",
                        "~/Scripts/jquery.blockUI.js",
                        "~/Scripts/accordion.js",
                        "~/Scripts/common-script.js",
                        "~/Scripts/jquery.nicescroll.js",
                        "~/Scripts/plugins/data-tables/jquery.dataTables.js",
                        "~/Scripts/plugins/data-tables/DT_bootstrap.js",
                        "~/Scripts/plugins/data-tables/dynamic_table_init.js",
                        "~/Scripts/plugins/edit-table/edit-table.js",
                        "~/Content/flatdoc/legacy.js",
                        "~/Content/flatdoc/flatdoc.js",
                        "~/Content/flatdoc/theme-white/script.js",
                        "~/Scripts/jquery.multiple.select.js",
                        "~/Scripts/jquery.maskMoney.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
