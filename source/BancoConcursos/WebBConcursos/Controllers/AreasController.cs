﻿using Dominio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBConcursos.Models;

namespace WebBConcursos.Controllers
{
    [Authorize]
    public class AreasController : Controller
    {
        private readonly Repository repository;
        public AreasController(Repository Repository)
        {
            this.repository = Repository;
        }

        public ActionResult Index()
        {
            return View(PreencheModel());
        }

        private AreaViewModel PreencheModel()
        {
            int total = 0;
            IEnumerable<Area> areas = this.repository.AreaRepository.GetAll(x => x.Nome, 10, 1, out total);
            return new AreaViewModel(areas);
        }

        public ActionResult Listar()
        {
            return PartialView("_listar", PreencheModel());
        }

        public ActionResult Excluir(int id)
        {
            Area area = this.repository.AreaRepository.GetById(id);
            if (area != null)
            {
                AreaModel model = new AreaModel(area);
                return PartialView("_excluir", model);
            }
            return PartialView("_excluir", null);
        }

        [HttpPost]
        public ActionResult ExcluirFinal(int id)
        {
            if (ModelState.IsValid)
            {
                Area area = this.repository.AreaRepository.GetById(id);
                if (area != null)
                {
                    this.repository.AreaRepository.Delete(area);
                    this.repository.Save();
                }
                return View("_listagem", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }

        public ActionResult Editar(int? id)
        {
            if (id.HasValue)
            {
                Area area = this.repository.AreaRepository.GetById(id.Value);
                if (area != null)
                {
                    AreaModel model = new AreaModel(area);
                    return PartialView("_editar", model);
                }
            }
            return PartialView("_editar", null);
        }

        [HttpPost]
        public ActionResult Editar(AreaModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.AreaId.HasValue)
                {
                    Area area = this.repository.AreaRepository.GetById(model.AreaId.Value);
                    if (area != null)
                    {
                        area.Alterar(model.Nome);
                        this.repository.AreaRepository.Update(area);
                        this.repository.Save();
                    }
                }
                else
                {
                    Area area = new Area(model.Nome);
                    this.repository.AreaRepository.Add(area);
                    this.repository.Save();
                }

                return View("_listar", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }
	}
}