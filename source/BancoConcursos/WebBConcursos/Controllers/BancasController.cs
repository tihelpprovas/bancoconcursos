﻿using Dominio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBConcursos.Models;

namespace WebBConcursos.Controllers
{
    [Authorize]
    public class BancasController : Controller
    {
        private readonly Repository repository;
        public BancasController(Repository Repository)
        {
            this.repository = Repository;
        }
        public ActionResult Index()
        {
            return View(PreencheModel());
        }

        private BancaViewModel PreencheModel()
        {
            int total = 0;
            IEnumerable<BancaExaminadora> bancas = this.repository.BancaExaminadoraRepository.GetAll(x => x.Nome, 10, 1, out total);
            return new BancaViewModel(bancas);
        }

        public ActionResult Listar()
        {
            return PartialView("_listar", PreencheModel());
        }

        public ActionResult Excluir(int id)
        {
            BancaExaminadora banca = this.repository.BancaExaminadoraRepository.GetById(id);
            if (banca != null)
            {
                BancaModel model = new BancaModel(banca);
                return PartialView("_excluir", model);
            }
            return PartialView("_excluir", null);
        }

        [HttpPost]
        public ActionResult ExcluirFinal(int id)
        {
            if (ModelState.IsValid)
            {
                BancaExaminadora banca = this.repository.BancaExaminadoraRepository.GetById(id);
                if (banca != null)
                {
                    this.repository.BancaExaminadoraRepository.Delete(banca);
                    this.repository.Save();
                }
                return View("_listar", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }

        public ActionResult Editar(int? id)
        {
            if (id.HasValue)
            {
                BancaExaminadora banca = this.repository.BancaExaminadoraRepository.GetById(id.Value);
                if (banca != null)
                {
                    BancaModel model = new BancaModel(banca);
                    return PartialView("_editar", model);
                }
            }
            return PartialView("_editar", null);
        }

        [HttpPost]
        public ActionResult Editar(BancaModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.BancaId.HasValue)
                {
                    BancaExaminadora banca = this.repository.BancaExaminadoraRepository.GetById(model.BancaId.Value);
                    if (banca != null)
                    {
                        banca.Alterar(model.Nome, model.Sigla, model.Tags);
                        this.repository.BancaExaminadoraRepository.Update(banca);
                        this.repository.Save();
                    }
                }
                else
                {
                    BancaExaminadora banca = new BancaExaminadora(model.Nome, model.Sigla, model.Tags);
                    this.repository.BancaExaminadoraRepository.Add(banca);
                    this.repository.Save();
                }

                return View("_listar", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }

	}
}