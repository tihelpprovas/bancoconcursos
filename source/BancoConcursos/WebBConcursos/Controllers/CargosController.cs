﻿using Dominio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBConcursos.Models;

namespace WebBConcursos.Controllers
{
    [Authorize]
    public class CargosController : Controller
    {
        private readonly Repository repository;

        public CargosController(Repository Repository)
        {
            this.repository = Repository;
        }
        public ActionResult Index()
        {
            return View(PreencheModel());
        }

        private CargoViewModel PreencheModel()
        {
            int total = 0;
            IEnumerable<Cargo> cargos = this.repository.CargoRepository.GetAll(x => x.Nome, 2000, 1, out total);
            return new CargoViewModel(cargos);
        }

        public ActionResult Listar()
        {
            return PartialView("_listagem", PreencheModel());
        }

        public ActionResult Excluir(int id)
        {
            Cargo cargo = this.repository.CargoRepository.GetById(id);
            if (cargo != null)
            {
                CargoModel model = new CargoModel(cargo);
                return PartialView("_excluir", model);
            }
            return PartialView("_excluir", null);
        }

        [HttpPost]
        public ActionResult ExcluirFinal(int id)
        {
            if (ModelState.IsValid)
            {
                Cargo cargo = this.repository.CargoRepository.GetById(id);
                if (cargo != null)
                {
                    this.repository.CargoRepository.Delete(cargo);
                    this.repository.Save();
                }
                return View("_listagem", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }

        public ActionResult Editar(int? id)
        {
            ViewBag.Escolaridades = this.repository.EscolaridadeRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.EscolaridadeId.ToString(), "Selecione", "");

            if (id.HasValue)
            {
                Cargo cargo = this.repository.CargoRepository.GetById(id.Value);
                if (cargo != null)
                {
                    CargoModel model = new CargoModel(cargo);
                    return PartialView("_editar", model);
                }
            }
            return PartialView("_editar", new CargoModel());
        }

        [HttpPost]
        public ActionResult Editar(CargoModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.CargoId.HasValue)
                {
                    Cargo cargo = this.repository.CargoRepository.GetById(model.CargoId.Value);
                    if (cargo != null)
                    {
                        cargo.Alterar(model.Nome, model.Salario, model.CargaHoraria, model.EscolaridadeId);
                        this.repository.CargoRepository.Update(cargo);
                        this.repository.Save();
                    }
                }
                else
                {
                    Cargo cargo = new Cargo(model.Nome, model.Salario, model.CargaHoraria, model.EscolaridadeId);
                    this.repository.CargoRepository.Add(cargo);
                    this.repository.Save();
                }
                
                return View("_listagem", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }
    }
}