﻿using Dominio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBConcursos.Models;

namespace WebBConcursos.Controllers
{
    [Authorize]
    public class ConcursosController : Controller
    {
        private readonly Repository repository;
        public ConcursosController(Repository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(PreencheModel());
        }

        private ConcursoViewModel PreencheModel()
        {
            int total = 0;
            IEnumerable<Concurso> concursos = this.repository.ConcursoRepository.GetAll(x => x.DataCadastro, 100, 1, out total);

            return new ConcursoViewModel(concursos);
        }

        public ActionResult Listar()
        {
            return PartialView("_listar", PreencheModel());
        }

        public ActionResult ListarCargos()
        {
            return PartialView("_concursoCargo");
        }

        [HttpPost]
        public ActionResult AdicionarCargo(ConcursoCargoModel cargoModel)
        {
            return RedirectToAction("Index");
        }

        public ActionResult Excluir(int id)
        {
            Concurso concurso = this.repository.ConcursoRepository.GetById(id);
            if (concurso != null)
            {
                ConcursoModel model = new ConcursoModel(concurso);
                return PartialView("_excluir", model);
            }
            return PartialView("_excluir", null);
        }

        [HttpPost]
        public ActionResult ExcluirFinal(int id)
        {
            if (ModelState.IsValid)
            {
                Concurso concurso = this.repository.ConcursoRepository.GetById(id);
                if (concurso != null)
                {
                    this.repository.ConcursoRepository.Delete(concurso);
                    this.repository.Save();
                }
                return View("_listar", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }

        public ActionResult Editar(int? id)
        {
            ViewBag.Instituicoes = this.repository.InstituicaoRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.InstituicaoId.ToString(), "Selecione", "");
            ViewBag.Bancas = this.repository.BancaExaminadoraRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.BancaExaminadoraId.ToString(), "Selecione", "");
            ViewBag.Cargos = this.repository.CargoRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.NomeTotal, x => x.CargoId.ToString(), "Cargo", "");
            ViewBag.Escolaridades = this.repository.EscolaridadeRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.EscolaridadeId.ToString(), "Escolaridade", "");
            ViewBag.Situacoes = new Concurso().Situacoes().ToSelectList(t => t.Text, x => x.Value.ToString(), "Status", "");

            int estadoSelecionado = 0;
            if (id.HasValue)
            {
                Concurso concurso = this.repository.ConcursoRepository.GetById(id.Value);
                if (concurso != null)
                {
                    if (concurso.Estados != null)
                    {
                        List<int> estadosSelecionados = concurso.Estados.Select(f => f.EstadoId).ToList();
                        ViewBag.ListaDeEstados = this.repository.EstadoRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.EstadoId.ToString(), estadosSelecionados);
                        if (estadosSelecionados.Count.Equals(1))
                            estadoSelecionado = estadosSelecionados[0];
                    }

                    if (concurso.Cidades != null)
                    {
                        if (!estadoSelecionado.Equals(0))
                        {
                            List<int> cidadesSelecionadas = concurso.Cidades.Select(f => f.CidadeId).ToList();
                            ViewBag.ListaDeCidades = this.repository.CidadeRepository.GetMany(x => x.EstadoId.Equals(estadoSelecionado)).OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.CidadeId.ToString(), cidadesSelecionadas);
                        }
                        else
                        {
                            ViewBag.ListaDeCidades = concurso.Cidades.ToSelectList(t => t.Nome, x => x.EstadoId.ToString());
                        }
                    }

                    ConcursoModel model = new ConcursoModel(concurso);
                    return PartialView("_editar", model);
                }
            }
            else
            {
                ViewBag.ListaDeEstados = this.repository.EstadoRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.EstadoId.ToString());
                ViewBag.ListaDeCidades = new List<SelectListItem>();
            }
            return PartialView("_editar", new ConcursoModel());
        }

        [HttpPost]
        public ActionResult Editar(ConcursoModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.ConcursoId.HasValue)
                {
                    Concurso concurso = this.repository.ConcursoRepository.GetById(model.ConcursoId.Value);
                    if (concurso != null)
                    {
                        concurso.Alterar(model.Descricao, model.Ano, model.QtdInscritos, model.Tags, (enmStatusConcurso)model.Status, model.DataValidadeConcurso, model.DataCancelamento, model.InstituicaoId, model.BancaExaminadoraId);
                        this.repository.ConcursoRepository.Update(concurso);

                        // Concurso Cargos
                        if (model.Cargos != null)
                        {
                            foreach (ConcursoCargoModel item in model.Cargos)
                            {
                                if (item.Acao == "adicionar")
                                {
                                    ConcursoCargo concursoCargo = new ConcursoCargo(concurso, item.CargoId, item.EscolaridadeId, item.Vagas, item.CadastroReserva, item.VagasDeficientes, item.VagasNegros, item.TxInscricao, item.Salario, item.CargaHoraria, item.DataProvaObjetiva1, item.DataProvaObjetiva2, item.DataProvaPratica, item.DataProvaTitulos, item.DataProvaTAF, item.Horista);
                                    this.repository.ConcursoCargoRepository.Add(concursoCargo);

                                    IncrementaCargo(item.CargoId);
                                }
                                else if (item.Acao == "alterar")
                                {
                                    ConcursoCargo concursoCargo = this.repository.ConcursoCargoRepository.GetById(item.ConcursoCargoId.Value);
                                    concursoCargo.Alterar(item.CargoId, item.EscolaridadeId, item.Vagas, item.CadastroReserva, item.VagasDeficientes, item.VagasNegros, item.TxInscricao, item.Salario, item.CargaHoraria, item.DataProvaObjetiva1, item.DataProvaObjetiva2, item.DataProvaPratica, item.DataProvaTitulos, item.DataProvaTAF, item.Horista);
                                    this.repository.ConcursoCargoRepository.Update(concursoCargo);

                                    IncrementaCargo(item.CargoId);
                                }
                                else if (item.Acao == "excluir" && item.ConcursoCargoId.HasValue)
                                {
                                    ConcursoCargo concursoCargo = this.repository.ConcursoCargoRepository.GetById(item.ConcursoCargoId.Value);
                                    if (concurso != null)
                                        this.repository.ConcursoCargoRepository.Delete(concursoCargo);
                                }
                            }
                        }

                        // Concurso Previsto
                        if (model.ConcursoPrevisto != null && model.ConcursoPrevisto.ConcursoPrevistoId.HasValue)
                        {
                            ConcursoPrevisto concursoPrevisto = this.repository.ConcursoPrevistoRepository.GetById(model.ConcursoPrevisto.ConcursoPrevistoId.Value);
                            if(concursoPrevisto != null)
                            {
                                concursoPrevisto.Alterar(model.ConcursoPrevisto.Especulacao,
                                    model.ConcursoPrevisto.Solicitado,
                                    model.ConcursoPrevisto.Autorizado,
                                    model.ConcursoPrevisto.DataEspeculacao,
                                    model.ConcursoPrevisto.DataSolicitacao,
                                    model.ConcursoPrevisto.DataAutorizacao);
                                this.repository.ConcursoPrevistoRepository.Update(concursoPrevisto);
                            }
                        }
                        else if (model.ConcursoPrevisto != null)
                        {
                            ConcursoPrevisto concursoPrevisto = new ConcursoPrevisto(concurso, model.ConcursoPrevisto.Especulacao, model.ConcursoPrevisto.Solicitado, model.ConcursoPrevisto.Autorizado, model.ConcursoPrevisto.DataEspeculacao, model.ConcursoPrevisto.DataSolicitacao, model.ConcursoPrevisto.DataAutorizacao);
                            this.repository.ConcursoPrevistoRepository.Add(concursoPrevisto);
                        }

                        // Concurso Edital Publicado
                        if(model.ConcursoEditalPublicado != null && model.ConcursoEditalPublicado.ConcursoEditalPublicadoId.HasValue)
                        {
                            ConcursoEditalPublicado concursoEditalPublicado = this.repository.ConcursoEditalPublicadoRepository.GetById(model.ConcursoEditalPublicado.ConcursoEditalPublicadoId.Value);
                            if(concursoEditalPublicado != null)
                            {
                                concursoEditalPublicado.Alterar(model.ConcursoEditalPublicado.DataInicioInscricao.Value, model.ConcursoEditalPublicado.DataFimInscricao.Value);
                                this.repository.ConcursoEditalPublicadoRepository.Update(concursoEditalPublicado);
                            }
                        }
                        else if (model.ConcursoEditalPublicado != null && model.ConcursoEditalPublicado.DataInicioInscricao.HasValue && model.ConcursoEditalPublicado.DataFimInscricao.HasValue)
                        {
                            ConcursoEditalPublicado concursoEditalPublicado = new ConcursoEditalPublicado(concurso, model.ConcursoEditalPublicado.DataInicioInscricao.Value, model.ConcursoEditalPublicado.DataFimInscricao.Value);
                            this.repository.ConcursoEditalPublicadoRepository.Add(concursoEditalPublicado);
                        }

                        // Estados
                        if (model.Estados != null)
                        {
                            concurso.RemoveEstados();

                            foreach (int item in model.Estados)
                            {
                                Estado estado = this.repository.EstadoRepository.GetById(item);
                                if (estado != null)
                                    concurso.AdicionaEstado(estado);
                            }
                            this.repository.ConcursoRepository.Update(concurso);
                        }

                        // Cidades
                        if(model.Cidades != null)
                        {
                            concurso.RemoveCidades();

                            foreach (int item in model.Cidades)
                            {
                                Cidade cidade = this.repository.CidadeRepository.GetById(item);
                                if (cidade != null)
                                    concurso.AdicionaCidade(cidade);
                            }
                            this.repository.ConcursoRepository.Update(concurso);
                        }

                        this.repository.Save();
                    }
                }
                else
                {
                    Concurso concurso = new Concurso(model.Descricao, model.Ano, model.QtdInscritos, model.Tags, (enmStatusConcurso)model.Status, model.DataValidadeConcurso, model.DataCancelamento, model.InstituicaoId, model.BancaExaminadoraId);
                    this.repository.ConcursoRepository.Add(concurso);
                    this.repository.Save();

                    if (model.Cargos != null)
                    {
                        foreach (ConcursoCargoModel item in model.Cargos)
                        {
                            ConcursoCargo concursoCargo = new ConcursoCargo(concurso, item.CargoId, item.EscolaridadeId, item.Vagas, item.CadastroReserva, item.VagasDeficientes, item.VagasNegros, item.TxInscricao, item.Salario, item.CargaHoraria, item.DataProvaObjetiva1, item.DataProvaObjetiva2, item.DataProvaPratica, item.DataProvaTitulos, item.DataProvaTAF, item.Horista);
                            this.repository.ConcursoCargoRepository.Add(concursoCargo);

                            IncrementaCargo(item.CargoId);
                        }
                    }

                    // Concurso Previsto
                    ConcursoPrevisto concursoPrevisto = new ConcursoPrevisto(concurso, model.ConcursoPrevisto.Especulacao, model.ConcursoPrevisto.Solicitado, model.ConcursoPrevisto.Autorizado, model.ConcursoPrevisto.DataEspeculacao, model.ConcursoPrevisto.DataSolicitacao, model.ConcursoPrevisto.DataAutorizacao);
                    this.repository.ConcursoPrevistoRepository.Add(concursoPrevisto);

                    // Estados
                    if (model.Estados != null)
                    {
                        foreach (int item in model.Estados)
                        {
                            Estado estado = this.repository.EstadoRepository.GetById(item);
                            if (estado != null)
                                concurso.AdicionaEstado(estado);
                        }
                        this.repository.ConcursoRepository.Update(concurso);
                    }

                    //Cidades
                    if(model.Cidades != null)
                    {
                        foreach (int item in model.Cidades)
                        {
                            Cidade cidade = this.repository.CidadeRepository.GetById(item);
                            if (cidade != null)
                                concurso.AdicionaCidade(cidade);
                        }
                        this.repository.ConcursoRepository.Update(concurso);
                    }

                    this.repository.Save();
                }

                return View("_listar", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }

        private void IncrementaCargo(int cargoId)
        {
            Cargo cargo = this.repository.CargoRepository.GetById(cargoId);
            if (cargo != null)
            {
                cargo.IncrementaUso();
                this.repository.CargoRepository.Update(cargo);
            }
        }

    }
}