﻿using Dominio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBConcursos.Models;

namespace WebBConcursos.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
       

        private readonly Repository repository;
        public HomeController(Repository Repository)
        {
            this.repository = Repository;
        }

        public ActionResult Index()
        {
            //Administrador admin = new Administrador("Giovani Carlos de Andrade", "giovani@helpprovas.com.br", "HelP-2015".ToCrypt());
            //this.repository.AdministradorRepository.Add(admin);
            //this.repository.Save();

            //decimal valor = 654321;
            //string valorS = string.Format("{0:c}", valor);

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}