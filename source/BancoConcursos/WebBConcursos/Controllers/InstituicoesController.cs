﻿using Dominio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBConcursos.Models;

namespace WebBConcursos.Controllers
{
    [Authorize]
    public class InstituicoesController : Controller
    {
        private readonly Repository repository;
        public InstituicoesController(Repository Repository)
        {
            this.repository = Repository;
        }
        public ActionResult Index()
        {
            return View(PreencheModel());
        }

        private InstituicaoViewModel PreencheModel()
        {
            int total = 0;
            IEnumerable<Instituicao> instituicoes = this.repository.InstituicaoRepository.GetMany(x => x.Desativado != true, x => x.Nome, 1000, 1, out total);
            return new InstituicaoViewModel(instituicoes);
        }

        public ActionResult Listar()
        {
            return PartialView("_listar", PreencheModel());
        }

        public ActionResult Excluir(int id)
        {
            Instituicao instituicao = this.repository.InstituicaoRepository.GetById(id);
            if (instituicao != null)
            {
                InstituicaoModel model = new InstituicaoModel(instituicao);
                return PartialView("_excluir", model);
            }
            return PartialView("_excluir", null);
        }

        [HttpPost]
        public ActionResult ExcluirFinal(int id)
        {
            if (ModelState.IsValid)
            {
                Instituicao instituicao = this.repository.InstituicaoRepository.GetById(id);
                if (instituicao != null)
                {
                    instituicao.Desativar();
                    this.repository.Save();
                }
                return View("_listar", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }

        public ActionResult Editar(int? id)
        {
            List<SelectListItem> listEstados = this.repository.EstadoRepository.GetAll().OrderBy(x => x.Nome).ToSelectList(t => t.Nome, x => x.EstadoId.ToString(), "Selecione", "");
            ViewBag.Estados = listEstados;
            ViewBag.Areas = this.repository.AreaRepository.GetAll().OrderBy(x => x.Nome).ToList().ToSelectList(x => x.Nome, y => y.AreaId.ToString(), "Selecione", "");
            List<SelectListItem> listCidades = MVCExtensoes.SelectListVazio();
            InstituicaoModel model = new InstituicaoModel();
            if (id.HasValue)
            {
                Instituicao instituicao = this.repository.InstituicaoRepository.GetById(id.Value);
                if (instituicao != null)
                {
                    if(instituicao.Estado != null)
                        listCidades = this.repository.CidadeRepository.GetMany(x => x.EstadoId == instituicao.EstadoId).ToSelectList(c => c.Nome, c => c.CidadeId.ToString(), "Selecione", "");
                    
                    model = new InstituicaoModel(instituicao);
                }
            }

            ViewBag.ListaDeCidades = listCidades;
            return PartialView("_editar", model);
        }

        [HttpPost]
        public ActionResult Editar(InstituicaoModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.InstituicaoId.HasValue)
                {
                    Instituicao instituicao = this.repository.InstituicaoRepository.GetById(model.InstituicaoId.Value);
                    if (instituicao != null)
                    {
                        instituicao.Alterar(model.Nome, model.Sigla, model.TextoDescritivo, model.Tags, model.AreaId, model.CidadeId, model.EstadoId, model.Url, model.Logradouro, model.Numero, model.Bairro);
                        this.repository.InstituicaoRepository.Update(instituicao);
                        this.repository.Save();
                    }
                }
                else
                {
                    Instituicao instituicao = new Instituicao(model.Nome, model.Sigla, model.TextoDescritivo, model.Tags, model.AreaId, model.CidadeId, model.EstadoId, model.Url, model.Logradouro, model.Numero, model.Bairro);
                    this.repository.InstituicaoRepository.Add(instituicao);
                    this.repository.Save();
                }

                return View("_listar", PreencheModel());
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }
	}
}