﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebBancoConcursos.Models;

namespace WebBancoConcursos.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult RenderLogin()
        {
            return View("_LoginPartial", new LoginViewModel());
        }

        [HttpPost]
        public JsonResult Index(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(login.Usuario, login.Senha)) 
                {
                    return Json(new
                    {
                        Url = Url.Action("Index", "Home"), 
                        Erro = false
                    });
                }
                else
                {
                    return Json(new { Msg = "Login e/ou Senha inválidos!", Erro = true });
                }
            }
            else
            {
                return Json(new { Msg = "Preencha todos os campos!", Erro = true });
            }
        }
    }
}