﻿using Dominio;
using Infra.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebBConcursos.Controllers
{
    public class UtilController : Controller
    {
        private readonly Repository repository;
        public UtilController(Repository Repository)
        {
            this.repository = Repository;
        }

        public JsonResult _GetCidadesPorIdEstado(int idEstado)
        {
            Dictionary<string, string> dicCidades = this.repository.CidadeRepository.GetMany(x => x.EstadoId.Equals(idEstado)).OrderBy(x => x.Nome).ToDictionary(x => x.CidadeId.ToString(), x => x.Nome);
            return Json(dicCidades.ToList(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult _GetEstadoInstituicao(int idInstituicao)
        {
            int _estado = 0;
            int _cidade = 0;
            string _nomeCidade = string.Empty;
            Instituicao instituicao = this.repository.InstituicaoRepository.GetById(idInstituicao);
            if (instituicao != null)
            { 
                if(instituicao.EstadoId.HasValue)
                    _estado = instituicao.EstadoId.Value;
                if (instituicao.CidadeId.HasValue)
                {
                    _cidade = instituicao.CidadeId.Value;
                    _nomeCidade = instituicao.Cidade.Nome;
                }
            }

            return Json(new { estado = _estado, cidade = _cidade, nomeCidade = _nomeCidade }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult _GetDadosCargo(int idCargo)
        {
            int _escolaridadeId = 0;
            string _salario = string.Empty;
            int _cargaHoraria = 0;
            Cargo cargo = this.repository.CargoRepository.GetById(idCargo);
            if (cargo != null)
            {
                if (cargo.EscolaridadeId.HasValue)
                    _escolaridadeId = cargo.EscolaridadeId.Value;
                if (cargo.Salario.HasValue)
                    _salario = string.Format("{0:N2}", cargo.Salario);
                if (cargo.CargaHoraria.HasValue)
                    _cargaHoraria = cargo.CargaHoraria.Value;
            }

            return Json(new { EscolaridadeId = _escolaridadeId, Salario = _salario, CargaHoraria = _cargaHoraria }, JsonRequestBehavior.AllowGet);
        }
	}
}