﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class AreaViewModel
    {
        public List<AreaModel> Areas { get; set; }

        public AreaViewModel()
        {
            this.Areas = new List<AreaModel>();
        }

        public AreaViewModel(IEnumerable<Area> Areas)
            : this()
        {
            PreencherModel(Areas);
        }

        private void PreencherModel(IEnumerable<Area> Areas)
        {
            if (this.Areas == null)
                this.Areas = new List<AreaModel>();
            foreach (Area Area in Areas)
            {
                this.Areas.Add(new AreaModel(Area.AreaId, Area.Nome));
            }
        }
    }

    public class AreaModel
    {
        public int? AreaId { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        public string Nome { get; set; }

        public AreaModel()
        {

        }

        public AreaModel(int? idArea, string nome)
        {
            this.AreaId = idArea;
            this.Nome = nome;
        }

        public AreaModel(Area Area)
        {
            this.AreaId = Area.AreaId;
            this.Nome = Area.Nome;
        }
    }
}