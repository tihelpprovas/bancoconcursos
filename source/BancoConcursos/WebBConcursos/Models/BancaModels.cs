﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class BancaViewModel
    {
        public List<BancaModel> Bancas { get; set; }

        public BancaViewModel()
        {
            this.Bancas = new List<BancaModel>();
        }

        public BancaViewModel(IEnumerable<BancaExaminadora> bancas)
            : this()
        {
            PreencherModel(bancas);
        }

        private void PreencherModel(IEnumerable<BancaExaminadora> bancas)
        {
            foreach (BancaExaminadora banca in bancas)
            {
                this.Bancas.Add(new BancaModel(banca));
            }
        }
    }

    public class BancaModel
    {
        public int? BancaId { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        [StringLength(500)]
        public string Nome { get; set; }

        [StringLength(50)]
        public string Sigla { get; set; }

        public string Tags { get; set; }


        public BancaModel()
        {

        }

        public BancaModel(int? idBanca, string nome, string sigla, string tags)
        {
            this.BancaId = idBanca;
            this.Nome = nome;
            this.Sigla = sigla;
            this.Tags = tags;
        }

        public BancaModel(BancaExaminadora banca)
            : this(banca.BancaExaminadoraId, banca.Nome, banca.Sigla, banca.Tags)
        {
        }
    }
}