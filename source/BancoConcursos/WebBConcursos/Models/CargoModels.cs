﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class CargoViewModel 
    {
        public List<CargoModel> Cargos { get; set; }

        public CargoViewModel()
        {
            this.Cargos = new List<CargoModel>();
        }

        public CargoViewModel(IEnumerable<Cargo> cargos)
            : this()
        {
            PreencherModel(cargos);
        }

        private void PreencherModel(IEnumerable<Cargo> cargos)
        {
            foreach (Cargo cargo in cargos)
            {
                this.Cargos.Add(new CargoModel(cargo));
            }
        }
    }

    public class CargoModel
    {
        public int? CargoId { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        public string Nome { get; set; }

        [Display(Name="Salário")]
        public Decimal? Salario { get; set; }

        [Display(Name="Carga Horária")]
        public int? CargaHoraria { get; set; }

        [Display(Name = "Escolaridade")]
        public int? EscolaridadeId { get; set; }

        public string NomeEscolaridade { get; set; }


        public CargoModel()
        {

        }

        public CargoModel(int? idCargo, string nome, Decimal? salario, int? cargaHoraria, int? escolaridadeId)
        {
            this.CargoId = idCargo;
            this.Nome = nome;
            this.Salario = salario;
            this.CargaHoraria = cargaHoraria;
            this.EscolaridadeId = escolaridadeId;
        }

        public CargoModel(Cargo cargo)
            : this(cargo.CargoId, cargo.Nome, cargo.Salario, cargo.CargaHoraria, cargo.EscolaridadeId)
        {
            if (cargo.Escolaridade != null)
                this.NomeEscolaridade = cargo.Escolaridade.Nome;
        }
    }
}