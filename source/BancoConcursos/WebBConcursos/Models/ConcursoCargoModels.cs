﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class ConcursoCargoViewModel
    {
        public List<ConcursoCargoModel> ConcursosCargos { get; set; }

        public ConcursoCargoViewModel()
        {
            this.ConcursosCargos = new List<ConcursoCargoModel>();
        }

        public ConcursoCargoViewModel(IEnumerable<ConcursoCargo> concursosCargos)
            : this()
        {
            PreencherModel(concursosCargos);
        }

        private void PreencherModel(IEnumerable<ConcursoCargo> concursosCargos)
        {
            foreach (ConcursoCargo concursoCargo in concursosCargos)
            {
                this.ConcursosCargos.Add(new ConcursoCargoModel(concursoCargo));
            }
        }
    }

    public class ConcursoCargoModel
    {
        public int? ConcursoCargoId { get; set; }
        public int CargoId { get; set; }
        public string NomeCargo { get; set; }
        public int EscolaridadeId { get; set; }
        public string NomeEscolaridade { get; set; }
        public int? Vagas { get; set; }
        public int? CadastroReserva { get; set; }
        public int? VagasDeficientes { get; set; }
        public int? VagasNegros { get; set; }
        public Decimal? TxInscricao { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:c}")]
        [DataType(DataType.Currency, ErrorMessage = "Valor inválido")]
        [Range(0.1, 100000000000000, ErrorMessage = "Valor não permitido")]
        public Decimal? Salario { get; set; }
        public int? CargaHoraria { get; set; }
        public DateTime? DataProvaObjetiva1 { get; set; }
        public DateTime? DataProvaObjetiva2 { get; set; } 
        public DateTime? DataProvaPratica { get; set; }
        public DateTime? DataProvaTitulos { get; set; }
        public DateTime? DataProvaTAF { get; set; }
        public bool Horista { get; set; }

        public string HoristaNome
        {
            get
            {
                if (this.Horista)
                    return "Hora";
                return "Mês";
            }
        }

        public string Acao { get; set; }

        public ConcursoCargoModel()
        {

        }

        public ConcursoCargoModel(int? concursoCargoId, 
            int cargoId,
            string nomeCargo,
            int escolaridadeId,
            string nomeEscolaridade,
            int? vagas, 
            int? cadastroReserva, 
            int? vagasDeficientes, 
            int? vagasNegros, 
            Decimal? txInscricao, 
            Decimal? salario, 
            int? cargaHoraria, 
            DateTime? dataProvaObjetiva1, 
            DateTime? dataProvaObjetiva2, 
            DateTime? dataProvaPratica, 
            DateTime? dataProvaTitulos, 
            DateTime? dataProvaTAF,
            bool horista)
        {
            this.ConcursoCargoId = concursoCargoId;
            this.CargoId = cargoId;
            this.NomeCargo = nomeCargo;
            this.EscolaridadeId = escolaridadeId;
            this.NomeEscolaridade = nomeEscolaridade;
            this.Vagas = vagas;
            this.CadastroReserva = cadastroReserva;
            this.VagasDeficientes = vagasDeficientes;
            this.VagasNegros = vagasNegros;
            this.TxInscricao = txInscricao;
            this.Salario = salario;
            this.CargaHoraria = cargaHoraria;
            this.DataProvaObjetiva1 = dataProvaObjetiva1;
            this.DataProvaObjetiva2 = dataProvaObjetiva2;
            this.DataProvaPratica = dataProvaPratica;
            this.DataProvaTitulos = dataProvaTitulos;
            this.DataProvaTAF = dataProvaTAF;
            this.Horista = horista;
        }

        public ConcursoCargoModel(ConcursoCargo concursoCargo)
            : this(concursoCargo.ConcursoCargoId, 
            concursoCargo.CargoId,
            (concursoCargo.Cargo != null) ? concursoCargo.Cargo.Nome : string.Empty,
            concursoCargo.EscolaridadeId,
            (concursoCargo.Escolaridade != null) ? concursoCargo.Escolaridade.Nome : string.Empty,
            concursoCargo.Vagas, 
            concursoCargo.CadastroReserva, 
            concursoCargo.VagasDeficientes, 
            concursoCargo.VagasNegros, 
            concursoCargo.TxInscricao, 
            concursoCargo.Salario, 
            concursoCargo.CargaHoraria, 
            concursoCargo.DataProvaObjetiva1, 
            concursoCargo.DataProvaObjetiva2, 
            concursoCargo.DataProvaPratica, 
            concursoCargo.DataProvaTitulos, 
            concursoCargo.DataProvaTAF,
            concursoCargo.Horista)
        {
        }

    }
}