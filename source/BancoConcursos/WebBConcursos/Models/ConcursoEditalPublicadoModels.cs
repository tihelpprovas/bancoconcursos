﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class ConcursoEditalPublicadoModel
    {
        public int? ConcursoEditalPublicadoId { get; set; }

        [Display(Name="Data Inicio da inscrição")]
        public DateTime? DataInicioInscricao { get; set; }

        [Display(Name = "Data Fim da inscrição")]
        public DateTime? DataFimInscricao { get; set; }

        public ConcursoEditalPublicadoModel()
        {

        }

        public ConcursoEditalPublicadoModel(int? concursoEditalPublicadoId, DateTime? dataInicioInscricao, DateTime? dataFimInscricao)
        {
            this.ConcursoEditalPublicadoId = concursoEditalPublicadoId;
            this.DataInicioInscricao = dataInicioInscricao;
            this.DataFimInscricao = dataFimInscricao;
        }

        public ConcursoEditalPublicadoModel(ConcursoEditalPublicado concursoEditalPublicado)
            : this(concursoEditalPublicado.ConcursoEditalPublicadoId, concursoEditalPublicado.DataInicioInscricao, concursoEditalPublicado.DataFimInscricao)
        {

        }
    }
}