﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class ConcursoViewModel
    {
        public List<ConcursoModel> Concursos { get; set; }

        public ConcursoViewModel()
        {
            this.Concursos = new List<ConcursoModel>();
        }

        public ConcursoViewModel(IEnumerable<Concurso> Concursos)
            : this()
        {
            PreencherModel(Concursos);
        }

        private void PreencherModel(IEnumerable<Concurso> Concursos)
        {
            foreach (Concurso concurso in Concursos)
            {
                this.Concursos.Add(new ConcursoModel(concurso));
            }
        }
    }

    public class ConcursoModel
    {
        public int? ConcursoId { get; set; }

        [Required(ErrorMessage="Campo obrigatório")]
        [Display(Name="Descrição")]
        public string Descricao { get; set; }

        public int? Ano { get; set; }

        [Display(Name = "Inscritos")]
        public int? QtdInscritos { get; set; }
        public string Tags { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        public byte Status { get; set; }
        public DateTime? DataValidadeConcurso { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime? DataAlteracao { get; set; }
        public DateTime? DataCancelamento { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        [Display(Name = "Instituição")]
        public int InstituicaoId { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        [Display(Name = "Banca examinadora")]
        public int BancaExaminadoraId { get; set; }

        [Display(Name = "Estados")]
        public List<int> Estados { get; set; }

        [Display(Name = "Cidades")]
        public List<int> Cidades { get; set; }

        public ConcursoPrevistoModel ConcursoPrevisto { get; set; }
        public ConcursoEditalPublicadoModel ConcursoEditalPublicado { get; set; }

        public List<ConcursoCargoModel> Cargos { get; set; }

        public ConcursoModel()
        {
            this.Ano = DateTime.Now.Year;
            this.Cargos = new List<ConcursoCargoModel>();
        }

        public ConcursoModel(int? idConcurso, string descricao, int? ano, int? qtdInscritos, string tags, byte status, DateTime? dataValidadeConcurso, DateTime dataCadastro, DateTime? dataAlteracao, DateTime? dataCancelamento, int idInstituicao, int idBancaExaminadora)
            : this()
        {
            this.ConcursoId = idConcurso;
            this.Descricao = descricao;
            this.Ano = ano;
            this.QtdInscritos = qtdInscritos;
            this.Tags = tags;
            this.Status = status;
            this.DataValidadeConcurso = dataValidadeConcurso;
            this.DataCadastro = dataCadastro;
            this.DataAlteracao = dataAlteracao;
            this.DataCancelamento = dataCancelamento;
            this.InstituicaoId = idInstituicao;
            this.BancaExaminadoraId = idBancaExaminadora;
        }

        public ConcursoModel(Concurso concurso)
            : this(concurso.ConcursoId,
            concurso.Descricao,
            concurso.Ano,
            concurso.QtdInscritos,
            concurso.Tags,
            (byte)concurso.Status,
            concurso.DataValidadeConcurso,
            concurso.DataCadastro,
            concurso.DataAlteracao,
            concurso.DataCancelamento,
            concurso.InstituicaoId,
            concurso.BancaExaminadoraId)
        {
            if (concurso.ConcursoPrevistoAtual != null)
                this.ConcursoPrevisto = new ConcursoPrevistoModel(concurso.ConcursoPrevistoAtual);
            if (concurso.ConcursoEditalPublicado != null)
                this.ConcursoEditalPublicado = new ConcursoEditalPublicadoModel(concurso.ConcursoEditalPublicado);
            if (concurso.Cargos != null && concurso.Cargos.Count > 0)
            {
                PreencherModel(concurso.Cargos);
            }
        }

        private void PreencherModel(IEnumerable<ConcursoCargo> Cargos)
        {
            foreach (ConcursoCargo concursoCargo in Cargos)
            {
                this.Cargos.Add(new ConcursoCargoModel(concursoCargo));
            }
        }
        
    }
}

