﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class ConcursoPrevistoModel
    {
        public int? ConcursoPrevistoId { get; set; }

        [Display(Name="Especulação?")]
        public bool Especulacao { get; set; }

        [Display(Name = "Solicitado?")]
        public bool Solicitado { get; set; }

        [Display(Name = "Autorizado?")]
        public bool Autorizado { get; set; }

        [Display(Name = "Data da especulação")]
        public DateTime? DataEspeculacao { get; set; }

        [Display(Name = "Data da solicitação")]
        public DateTime? DataSolicitacao { get; set; }

        [Display(Name = "Data da autorização")]
        public DateTime? DataAutorizacao { get; set; }

        public ConcursoPrevistoModel()
        {

        }

        public ConcursoPrevistoModel(int? concursoPrevistoId,
            bool especulacao,
            bool solicitado,
            bool autorizado,
            DateTime? dataEspeculacao,
            DateTime? dataSolicitacao,
            DateTime? dataAutorizacao)
        {
            this.ConcursoPrevistoId = concursoPrevistoId;
            this.Especulacao = especulacao;
            this.Solicitado = solicitado;
            this.Autorizado = autorizado;
            this.DataEspeculacao = dataEspeculacao;
            this.DataSolicitacao = dataSolicitacao;
            this.DataAutorizacao = dataAutorizacao;
        }

        public ConcursoPrevistoModel(ConcursoPrevisto concursoPrevisto)
            : this(concursoPrevisto.ConcursoPrevistoId,
            concursoPrevisto.Especulacao,
            concursoPrevisto.Solicitado,
            concursoPrevisto.Autorizado,
            concursoPrevisto.DataEspeculacao,
            concursoPrevisto.DataSolicitacao,
            concursoPrevisto.DataAutorizacao)
        {

        }
    }
}