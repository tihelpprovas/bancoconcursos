﻿using Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebBConcursos.Models
{
    public class InstituicaoViewModel
    {
        public List<InstituicaoModel> Instituicoes { get; set; }

        public InstituicaoViewModel()
        {
            this.Instituicoes = new List<InstituicaoModel>();
        }

        public InstituicaoViewModel(IEnumerable<Instituicao> Instituicoes)
            : this()
        {
            PreencherModel(Instituicoes);
        }

        private void PreencherModel(IEnumerable<Instituicao> Instituicoes)
        {
            foreach (Instituicao Instituicao in Instituicoes)
            {
                this.Instituicoes.Add(new InstituicaoModel(Instituicao));
            }
        }
    }

    public class InstituicaoModel
    {
        public int? InstituicaoId { get; set; }

        [Required(ErrorMessage="Campo obrigatório")]
        public string Nome { get; set; }
        public string Sigla { get; set; }

        [Display(Name = "Descrição")]
        public string TextoDescritivo { get; set; }
        public string Tags { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime? DataAlteracao { get; set; }

        [Display(Name = "Site")]
        public string Url { get; set; }

        [Display(Name = "Endereço")]
        public string Logradouro { get; set; }

        [Display(Name = "Número")]
        public string Numero { get; set; }
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Campo obrigatório")]
        [Display(Name = "Área")]
        public int AreaId { get; set; }

        [Display(Name = "Cidade")]
        public int? CidadeId { get; set; }

        [Display(Name = "Estado")]
        public int? EstadoId { get; set; }

        public InstituicaoModel()
        {
        }

        public InstituicaoModel(int? idInstituicao, string nome, string sigla, string textoDescritivo, string tags, DateTime dataCadastro, DateTime? dataAlteracao, int? cidadeId, int? estadoId, int areaId, string url, string logradouro, string numero, string bairro)
        {
            this.InstituicaoId = idInstituicao;
            this.Nome = nome;
            this.Sigla = sigla;
            this.TextoDescritivo = textoDescritivo;
            this.Tags = tags;
            this.DataCadastro = dataCadastro;
            this.DataAlteracao = dataAlteracao;
            this.AreaId = areaId;
            this.CidadeId = cidadeId;
            this.EstadoId = estadoId;
            this.Url = url;
            this.Logradouro = logradouro;
            this.Numero = numero;
            this.Bairro = bairro;
        }

        public InstituicaoModel(Instituicao Instituicao)
            : this(Instituicao.InstituicaoId, 
            Instituicao.Nome, 
            Instituicao.Sigla, 
            Instituicao.TextoDescritivo, 
            Instituicao.Tags, 
            Instituicao.DataCadastro, 
            Instituicao.DataAlteracao, 
            Instituicao.CidadeId, 
            Instituicao.EstadoId, 
            Instituicao.AreaId,
            Instituicao.Url,
            Instituicao.Logradouro,
            Instituicao.Numero,
            Instituicao.Bairro)
        {
        }
    }
}