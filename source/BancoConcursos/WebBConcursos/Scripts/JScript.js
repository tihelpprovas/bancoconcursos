﻿function Falha() {
    alertify.alert('<strong>Ocorreu um erro inesperado.</strong><br />Sistema indisponível no momento. Por favor, tente novamente mais tarde.');
    $("input").prop("disabled", null);
    $("select").prop("disabled", null);
    $("textarea").prop("disabled", null);
    $(".btn").prop("disabled", null);

}

function Carregando() {
    $('.alerta').remove();
    $("input").prop("disabled", "disabled");
    $("select").prop("disabled", "disabled");
    $("textarea").prop("disabled", "disabled");
    $(".btn").prop("disabled", "disabled");
}

function Sucesso(data) {
    if (!data.Erro) {
        if (!data.NaoLimpa) {
            $("input:text").val("");
            $("input:password").val("");
            $("select").val("");
            $("textarea").val("");
        }
        if (data.Msg) {
            alertify.alert(data.Msg);
        }
        else if (data.Ajax) {
            $.ajax({
                type: "POST",
                url: data.Ajax,
                success: function (data) {
                    Sucesso(data);
                },
                complete: function () {
                    Completo();
                },
                beforeSend: function () {
                    Carregando();
                },
                error: function () {
                    Falha();
                }
            });
        }
        else if (data.Modal) {
            $('.modal').modal('show');
        }
        else {
            if (data.Url) {
                window.location = data.Url;
            } else {
                window.location = ".";
            }
        }
    }
    else {
        if (data.Msg) {
            alertify.alert(data.Msg);
            $("input").prop("disabled", null);
            $("select").prop("disabled", null);
            $("textarea").prop("disabled", null);
            $(".btn").prop("disabled", null);
        }
        else {
            alertify.alert('<strong>Ocorreu um erro inesperado!</strong><br />Sistema indisponível no momento. Por favor, tente novamente mais tarde.');
        }
    }
    CenterItem(".alerta");
}

function Completo() {
    $("input").prop("disabled", null);
    $("select").prop("disabled", null);
    $("textarea").prop("disabled", null);
    $(".btn").prop("disabled", null);
    $(".disabled").prop("disabled", "disabled");
    fechaalerta();
}
function fechaalerta() {
    $('.alerta').remove();
    $('#modal-registro').modal('hide');
    $('#modal-delete').modal('hide');
}
function CenterItem(theItem) {
    var w = $(window);
    $(theItem).css("left", (w.width() - $(theItem).width()) / 2 + w.scrollLeft() + "px");
    $(theItem).attr("onclick", "$('" + theItem + "').fadeOut(500)");
}
//Menu zebrado
function zebrarMenu() {
    if ($(window).width() < 751) {
        $('.ul-zebrado li:odd').addClass('zebraUm');
        $('.ul-zebrado li:even').addClass('zebraDois');
    }
    else {
        $('.ul-zebrado li').removeClass('zebraUm zebraDois');
    }
}

function AbreModal(codigo, entidade) {
    if (codigo == "0")
        $('#myModalLabel').html('Criando ' + entidade);
    else {
        $('#myModalLabel').html('Editando ' + entidade + ': #' + codigo);
    }
    $('#modal-registro').modal('show');
}

$('#modal-registro').on('shown.bs.modal', function () {
    $('.focus').focus();
})

function AbreModalDelete() {
    $('#modal-delete').modal('show');
}

$(function () {
    $(".tamanhomax").keyup(function (event) {
        var target = $("#contador-regressivo");
        var max = target.attr('title');
        var len = $(this).val().length;
        var restante = max - len;
        if (len > max) {
            var val = $(this).val();
            $(this).val(val.substr(0, max));
            restante = 0;
        }
        target.html(restante);
    });

    $(window).resize(function () {
        zebrarMenu();
    });

    zebrarMenu();

    $(".center-block-passo").click(function () {
        $('.container-liberar-aula').effect("highlight", { color: "#facd59" }, 3000);
    });

    $('input[placeholder], textarea[placeholder]').placeholder();
    $(".novo-registro").click(function () {
        var entidade = $(this).attr("data-entity");
        $("#modal-result").load("/" + entidade + "/Editar/", function () { AbreModal(0, entidade); });
    });
});

function NovoRegistro()
{
    var entidade = $(".novo-registro").attr("data-entity");
    $("#modal-result").load("/" + entidade + "/Editar/", function () { AbreModal(0, entidade); });
}

function EditarRegistro(codigo, entidade) {
    $("#modal-result").load("/" + entidade + "/Editar/" + codigo, function () { AbreModal(codigo, entidade); });
}

function ExcluirRegistro(codigo, entidade) {
    $("#modal-result").load("/" + entidade + "/Excluir/" + codigo, function () { AbreModalDelete(); });
}

function HoristaNome(v)
{
    if (v == 'true')
        return "Hora";
    return "Mês";
}

function NovoCargo()
{
    if ($('#InputCargoId').val() == "")
    {
        alertify.alert('Informe o cargo!');
        return;
    }

    if ($('#InputEscolaridadeId').val() == "") {
        alertify.alert('Informe a escolaridade!');
        return;
    }

    var linha = "";
    linha += "<tr>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_CargoId_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].CargoId\" value=\"" + $('#InputCargoId').val() + "\" />" + $('#InputCargoId').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_EscolaridadeId_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].EscolaridadeId\" value=\"" + $('#InputEscolaridadeId').val() + "\" />" + $('#InputEscolaridadeId').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_Vagas_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].Vagas\" value=\"" + $('#InputVagas').val() + "\" />" + $('#InputVagas').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_CadastroReserva_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].CadastroReserva\" value=\"" + $('#InputCadastroReserva').val() + "\" />" + $('#InputCadastroReserva').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_VagasDeficientes_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].VagasDeficientes\" value=\"" + $('#InputVagasDeficientes').val() + "\" />" + $('#InputVagasDeficientes').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_VagasNegros_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].VagasNegros\" value=\"" + $('#InputVagasNegros').val() + "\" />" + $('#InputVagasNegros').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_TxInscricao_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].TxInscricao\" value=\"" + $('#InputTxInscricao').val() + "\" />" + $('#InputTxInscricao').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_Salario_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].Salario\" value=\"" + $('#InputSalario').val() + "\" />" + $('#InputSalario').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_Horista_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].Horista\" value=\"" + $('#InputHorista').val() + "\" />/" + HoristaNome($('#InputHorista').val()) + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_CargaHoraria_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].CargaHoraria\" value=\"" + $('#InputCargaHoraria').val() + "\" />" + $('#InputCargaHoraria').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_DataProvaObjetiva1_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].DataProvaObjetiva1\" value=\"" + $('#InputDataProvaObjetiva1').val() + "\" />" + $('#InputDataProvaObjetiva1').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_DataProvaObjetiva2_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].DataProvaObjetiva2\" value=\"" + $('#InputDataProvaObjetiva2').val() + "\" />" + $('#InputDataProvaObjetiva2').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_DataProvaPratica_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].DataProvaPratica\" value=\"" + $('#InputDataProvaPratica').val() + "\" />" + $('#InputDataProvaPratica').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_DataProvaTitulos_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].DataProvaTitulos\" value=\"" + $('#InputDataProvaTitulos').val() + "\" />" + $('#InputDataProvaTitulos').val() + "</td>";
    linha += "<td><input type=\"hidden\" id=\"Cargos_DataProvaTAF_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].DataProvaTAF\" value=\"" + $('#InputDataProvaTAF').val() + "\" />" + $('#InputDataProvaTAF').val() + "</td>";
    linha += "<td>&nbsp;<input type=\"hidden\" id=\"Cargos_Acao_" + indiceCargo + "\" name=\"Cargos[" + indiceCargo + "].Acao\" value=\"adicionar\" /></td>";
    linha += "</tr>";
    indiceCargo++;

    $('#list-cargos').append(linha);
    LimparNovoConcursoCargo();
}

var pressedCtrl = false;
document.onkeydown = function (e) {
    if (e.which == 17)
        pressedCtrl = true;
    //if(e.which == 83 && pressedCtrl == true) 
    //{
    //Aqui vai o código e chamadas de funções para o ctrl+s alert("CTRL + S pressionados"); 
    //}

    var aberto = ($("#modal-registro").data('bs.modal') || {}).isShown;
    if ((e.which == 107) && (!aberto))
        NovoRegistro();
}

function ExcluirCargoConcurso(op)
{
    var linha = "#tr-" + op;
    var acao = "#Cargos_Acao_" + op;
    var botao = "#chave_" + op;

    if($(acao).val() == "")
    {
        $(linha).attr("class", "tr-off");
        $(botao).attr("class", "ico-off");
        $(acao).val("excluir");
    }
    else if ($(acao).val() == "adicionar") { // Quando esta incluindo
        $(linha).attr("class", "tr-off");
        $(botao).attr("class", "ico-off");
        $(acao).val("");
    }
    else if ($(acao).val() == "excluir") {
        $(linha).attr("class", "tr-on");
        $(botao).attr("class", "ico-on");
        $(acao).val("");
    }
}

function EditarCargoConcurso(op)
{
    $('#InputPosicaoEditando').val(op);
    $('#InputConcursoCargoId').val($('#Cargos_ConcursoCargoId_' + op).val());
    $('#InputCargoId').val($('#Cargos_CargoId_' + op).val());
    $('#InputEscolaridadeId').val($('#Cargos_EscolaridadeId_' + op).val());
    $('#InputVagas').val($('#Cargos_Vagas_' + op).val());
    $('#InputCadastroReserva').val($('#Cargos_CadastroReserva_' + op).val());
    $('#InputVagasDeficientes').val($('#Cargos_VagasDeficientes_' + op).val());
    $('#InputVagasNegros').val($('#Cargos_VagasNegros_' + op).val());
    $('#InputTxInscricao').val($('#Cargos_TxInscricao_' + op).val());
    $('#InputSalario').val($('#Cargos_Salario_' + op).val());
    $('#InputHorista').val($('#Cargos_Horista_' + op).val());
    $('#InputCargaHoraria').val($('#Cargos_CargaHoraria_' + op).val());
    $('#InputDataProvaObjetiva1').val($('#Cargos_DataProvaObjetiva1_' + op).val());
    $('#InputDataProvaObjetiva2').val($('#Cargos_DataProvaObjetiva2_' + op).val());
    $('#InputDataProvaPratica').val($('#Cargos_DataProvaPratica_' + op).val());
    $('#InputDataProvaTitulos').val($('#Cargos_DataProvaTitulos_' + op).val());
    $('#InputDataProvaTAF').val($('#Cargos_DataProvaTAF_' + op).val());
    $('#salvar-editar, #cancelar-editar').show(function () {
        $('#novo-concurso-cargo').hide();
    });
}

function LimparNovoConcursoCargo()
{
    $('#InputPosicaoEditando, #InputConcursoCargoId, #InputEscolaridadeId, #InputCargoId, #InputVagas, #InputCadastroReserva, #InputVagasDeficientes, #InputVagasNegros, #InputTxInscricao, #InputSalario, #InputCargaHoraria, #InputDataProvaObjetiva1, #InputDataProvaObjetiva2, #InputDataProvaPratica, #InputDataProvaTitulos, #InputDataProvaTAF').val("");
    $('#InputHorista').val('false');
}

function CancelarEditar()
{
    LimparNovoConcursoCargo();

    $('#salvar-editar, #cancelar-editar').hide(function () {
        $('#novo-concurso-cargo').show();
    });
} 

function SalvarEditar()
{
    var p = $('#InputPosicaoEditando').val();
    var linha = "#tr-" + p;

    $('#Cargos_NomeCargo_' + p).html($('#InputCargoId option:selected').text());
    $('#Cargos_CargoId_' + p).val($('#InputCargoId').val());
    $('#Cargos_NomeEscolaridade_' + p).html($('#InputEscolaridadeId option:selected').text());
    $('#Cargos_EscolaridadeId_' + p).val($('#InputEscolaridadeId').val());
    $('#Cargos_Vagas_' + p).val($('#InputVagas').val());
    $('#Cargos_LabelVagas_' + p).html($('#InputVagas').val());
    $('#Cargos_CadastroReserva_' + p).val($('#InputCadastroReserva').val());
    $('#Cargos_LabelCadastroReserva_' + p).html($('#InputCadastroReserva').val());
    $('#Cargos_VagasDeficientes_' + p).val($('#InputVagasDeficientes').val());
    $('#Cargos_LabelVagasDeficientes_' + p).html($('#InputVagasDeficientes').val());
    $('#Cargos_VagasNegros_' + p).val($('#InputVagasNegros').val());
    $('#Cargos_LabelVagasNegros_' + p).html($('#InputVagasNegros').val());
    $('#Cargos_TxInscricao_' + p).val($('#InputTxInscricao').val());
    $('#Cargos_LabelTxInscricao_' + p).html($('#InputTxInscricao').val());
    $('#Cargos_Salario_' + p).val($('#InputSalario').val());
    $('#Cargos_LabelSalario_' + p).html($('#InputSalario').val());
    $('#Cargos_Horista_' + p).val($('#InputHorista').val());
    $('#Cargos_LabelHorista_' + p).html('/' + HoristaNome($('#InputHorista').val()));
    $('#Cargos_CargaHoraria_' + p).val($('#InputCargaHoraria').val());
    $('#Cargos_LabelCargaHoraria_' + p).html($('#InputCargaHoraria').val());
    $('#Cargos_DataProvaObjetiva1_' + p).val($('#InputDataProvaObjetiva1').val());
    $('#Cargos_LabelDataProvaObjetiva1_' + p).html($('#InputDataProvaObjetiva1').val());
    $('#Cargos_DataProvaObjetiva2_' + p).val($('#InputDataProvaObjetiva2').val());
    $('#Cargos_LabelDataProvaObjetiva2_' + p).html($('#InputDataProvaObjetiva2').val());
    $('#Cargos_DataProvaPratica_' + p).val($('#InputDataProvaPratica').val());
    $('#Cargos_LabelDataProvaPratica_' + p).html($('#InputDataProvaPratica').val());
    $('#Cargos_DataProvaTitulos_' + p).val($('#InputDataProvaTitulos').val());
    $('#Cargos_LabelDataProvaTitulos_' + p).html($('#InputDataProvaTitulos').val());
    $('#Cargos_DataProvaTAF_' + p).val($('#InputDataProvaTAF').val());
    $('#Cargos_LabelDataProvaTAF_' + p).html($('#InputDataProvaTAF').val()); 
    $('#Cargos_Acao_' + p).val("alterar");
    LimparNovoConcursoCargo();
    $('#salvar-editar, #cancelar-editar').hide(function () {
        $('#novo-concurso-cargo').show();
    });
    $(linha).attr("class", "tr-edit");
}