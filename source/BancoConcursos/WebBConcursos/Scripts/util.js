
$.util = {
    LoadMaskPhone: function (seletor) {

        $(seletor).mask("(99) 9999-9999");
        $(seletor).on('keyup', function () {
            var me = $(this);
            var value = me.val().replace(/\D+/g, '');
            if (value.length == 3) {

                switch (value) {
                    // S�o Paulo                
                    case '119':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '129':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '139':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '149':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '159':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '169':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '179':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '189':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '199':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    // Rio de Janeiro                
                    case '219':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '229':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '249':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    // Espitiro Santo                
                    case '279':
                        me.unmask().mask('(99) 99999-9999');
                        break;
                    case '289':
                        me.unmask().mask('(99) 99999-9999');
                        break;

                    default:
                        me.unmask().mask('(99) 9999-9999');
                }

                me.val(value).caret(5, 5).triggerHandler('input');
                me.triggerHandler('paste');
            }
        });
    },
    LoadMaskBornDate: function (seletor) {
        $(seletor).mask("99/99/9999");
    },
    LoadMaskCEP: function (seletor) {
        $(seletor).mask("99999-999");
    },
    LoadMakCPF: function (seletor) {
        $(seletor).mask("999.999.999-99");
    },
    loadMask: function (seletor, mascara) {
        $(seletor).mask(mascara);
    },
    bloqueiaTela: function (msg) {
        $.blockUI({
            message: msg,
            css: { backgroundColor: 'transparent', color: '#fff', border: 'none' }
        });
    },
    desbloqueiaTela: function () {
        $.unblockUI();
    },
    
    NotRepeatSpace: function (event) {

        var tecla = (window.event) ? event.keyCode : e.which;

        if (tecla == 32 && $('#nome-id').val().substr($('#nome-id').val().length - 1, $('#nome-id').val().length) == " ") {
            event.preventDefault();
        } else {
            return true;
        }

        return true;
    },
    EmailIsValid: function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};

