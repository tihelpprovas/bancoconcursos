﻿using Infra.CookieManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace System.Web.Mvc
{
    public static class MVCExtensoes
    {
        #region Hover

        public static string HoverAction(this HtmlHelper helper, string action)
        {
            if (helper.ViewContext.RequestContext.RouteData.Values["action"].Equals(action))
                return "active";
            return string.Empty;
        }

        public static string HoverControler(this HtmlHelper helper, string controller)
        {
            if (helper.ViewContext.RequestContext.RouteData.Values["controller"].Equals(controller))
                return "active";
            return string.Empty;
        }

        public static string Hover(this HtmlHelper helper, string action, string controller)
        {
            if (helper.ViewContext.RequestContext.RouteData.Values["controller"].Equals(controller) && helper.ViewContext.RequestContext.RouteData.Values["action"].Equals(action))
                return "active";
            return string.Empty;
        }

        #endregion

        #region Pegar Nome Cliente

        public static string NomeAlunoLogado(this HtmlHelper helper)
        {
            string retorno = "";
            string Nome = Cookie.GetCookie("pref", "NomeCliente");
            if (!string.IsNullOrEmpty(Nome))
                if (Nome.Contains(" "))
                    Nome = Nome.Substring(0, Nome.IndexOf(' '));
            return retorno + Nome;
        }

        #endregion

        #region Select List

        public static List<SelectListItem> ToSelectList<T>(
            this IEnumerable<T> enumerable,
            Func<T, string> text,
            Func<T, string> value,
            string defaultOption, string defaultOptionValue)
        {
            var items = enumerable.Select(f => new SelectListItem()
            {
                Text = text(f),
                Value = value(f)
            }).ToList();

            items.Insert(0, new SelectListItem()
            {
                Text = defaultOption,
                Value = defaultOptionValue
            });
            return items;
        }

        public static List<SelectListItem> ToSelectList<T>(
            this IEnumerable<T> enumerable,
            Func<T, string> text,
            Func<T, string> value,
            string selected)
        {
            var items = enumerable.Select(f => new SelectListItem()
            {
                Text = text(f),
                Value = value(f)
            }).ToList();

            items.Find(x => x.Value.Equals(selected)).Selected = true;

            return items;
        }

        public static List<SelectListItem> ToSelectList<T>(
            this IEnumerable<T> enumerable,
            Func<T, string> text,
            Func<T, string> value)
        {
            var items = enumerable.Select(f => new SelectListItem()
            {
                Text = text(f),
                Value = value(f)
            }).ToList();
            return items;
        }

        public static List<SelectListItem> SelectListVazio()
        {
            List<SelectListItem> itens = new List<SelectListItem>();
            itens.Add(new SelectListItem { Text = "Selecione", Value = "" });
            return itens;
        }

        public static List<SelectListItem> SelectListSimNao(bool Verdadeiro)
        {
            List<SelectListItem> itens = new List<SelectListItem>();
            if (Verdadeiro)
            {
                itens.Add(new SelectListItem { Text = "Sim", Value = "true", Selected = true });
                itens.Add(new SelectListItem { Text = "Não", Value = "false" });
            }
            else
            {
                itens.Add(new SelectListItem { Text = "Sim", Value = "true" });
                itens.Add(new SelectListItem { Text = "Não", Value = "false", Selected = true });
            }
            return itens;
        }

        public static List<SelectListItem> SelectListHorista(bool EhHorista)
        {
            List<SelectListItem> itens = new List<SelectListItem>();
            if (EhHorista)
            {
                itens.Add(new SelectListItem { Text = "Mês", Value = "false" });
                itens.Add(new SelectListItem { Text = "Hora", Value = "true", Selected = true });
            }
            else
            {
                itens.Add(new SelectListItem { Text = "Mês", Value = "false", Selected = true });
                itens.Add(new SelectListItem { Text = "Hora", Value = "true" });
            }
            return itens;
        }
        
        public static List<SelectListItem> ToSelectList<T>(
            this IEnumerable<T> enumerable,
            Func<T, string> text,
            Func<T, string> value,
            List<int> enumerableSelecionados)
        {
            var items = enumerable.Select(f => new SelectListItem()
            {
                Text = text(f),
                Value = value(f),
                Selected = enumerableSelecionados.Contains(Convert.ToInt32(value(f)))
            }).ToList();
            return items;
        }

        #endregion

        #region CheckBoxlist

        public static MvcHtmlString CheckBoxList(this HtmlHelper helper, string name,
            IEnumerable<SelectListItem> items, string checkBoxCssClass, string labelCssClass)
        {
            var output = new StringBuilder();
            var optionValue = string.Empty;
            foreach (var item in items)
            {
                optionValue = "name_" + item.Value;
                output.Append("<li><label for=\"" + optionValue + "\" class=\"" + labelCssClass + "\">" + item.Text);
                output.Append("<input type=\"checkbox\" id=\"" + optionValue + "\" name=\"MyOptions\" value=\"" + item.Value + "\" class=\"" + labelCssClass + "\"" + (item.Selected ? "checked" : "") + "/>");
                output.Append("</label></li>");
            }

            return new MvcHtmlString(output.ToString());
        }

        #endregion

        #region datas

        public static string ToShortDate(this DateTime? data)
        {
            if (data.HasValue)
                return data.Value.ToShortDateString();
            return string.Empty;
        }

        #endregion
    }

    public class SelectItemFull
    {
        public int Id { get; set; }
        public string Valor { get; set; }
        public bool Selecionado { get; set; }

        public SelectItemFull()
        {

        }

        public SelectItemFull(int id, string valor, bool selecionado)
        {
            this.Id = id;
            this.Valor = valor;
            this.Selecionado = selecionado;
        }
    }
}